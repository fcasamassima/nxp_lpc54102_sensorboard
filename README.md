# README #

Human activity recognition for the LC54102 MCU

### What is this repository for? ###

* This firmware can be used in combiantion with [LPC54102 Sensor Processing/Motion Solution](http://www.nxp.com/board/OM13078.html) to detect user's activity
* compiled using LPCXpresso 7.8.1 LPCOpen libraries version 2.14.1 available [here](https://www.lpcware.com/content/nxpfile/lpcopen-software-development-platform-lpc5410x-packages)
* Version 1.0


### How do I get set up? ###

* There are 3 folders, in each folder 2 projects, one for Core M0+ and one for Core M4
* The 3 configurations are: Kernel on Core M0 Only; Kernel on core M4 Only and dual core configuration
* Out of the 3 configurations, only one contains the classifier: the Dual Core, if you are interested to classifier, you can ignore the "M0 Kernel" and "M4 Kernel"
* The firmware has some modularity, some paramenters can be set using the file located in"Dual Core/Dual_Core_M0/global_inc/global_defines.h"
* It is possible to choose among: High data rate, and Normal Processing mode.
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact
