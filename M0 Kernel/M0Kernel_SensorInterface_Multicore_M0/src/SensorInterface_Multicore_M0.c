/*
===============================================================================
 Name        : SensorInterface_Multicore_M0.c
 Author      : $(author)
 Version     :
 Copyright   : $(copyright)
 Description : main definition
===============================================================================
*/

//#define DEBUG_PRINTF
#if defined (__USE_LPCOPEN)
#if defined(NO_BOARD_LIB)
#include "chip.h"
#else
#include "board.h"
#ifdef DEBUG_PRINTF
#include "uart_polling_m0.h"
#endif
#include "sensorhub.h"
#include "sensorhubBoard.h"
#include "sensors.h"
//#include "hostif.h"
#include "sensacq_i2c.h"
#include "stopwatch.h"
#include "global_types.h"
#include "GlobalVars.h"
#include "global_defines.h"

#endif
#endif

#include <cr_section_macros.h>

#if defined (__MULTICORE_MASTER_SLAVE_M0SLAVE) || \
    defined (__MULTICORE_MASTER_SLAVE_M4SLAVE)
#include "boot_multicore_slave.h"
#endif

int32_t inactivePeriod = 0;
uint8_t dataReady=0;

SensorStatus_t PysSensStatus[6];
PhysSensorCtrl_1_t PhysSensCtrl[6];
KernelTimer_Ctrl_t g_Timer;


// TODO: insert other definitions and declarations here
int cnt,cnt1;;
void SysTick_Handler(void)
{
	static int lowCnt = 0;
	cnt++;
	cnt1++;
	char dataStr[80];
	if (cnt < 50)
		Board_LED_Set(1, true);
	else
		Board_LED_Set(1, false);

	if (cnt >= 100) {
		dataReady =1;
		lowCnt++;
#ifdef DEBUG_PRINTF
//		sprintf(dataStr,"Packet %5d: AccX %5d - AccY %5d - AccZ %5d \r\n", lowCnt,
//		g_phySensors[0]->data16[0], g_phySensors[0]->data16[1], g_phySensors[0]->data16[2]);
//		uart_puts(dataStr);   //BF: added for AN example
//		uart_puts("Test out \r\n");
#endif

		cnt = 0;
	}

}

void HardFault_Handler(void){
	Board_LED_Set(1, false);
	Board_LED_Set(0, true);
#ifdef DEBUG_PRINTF
	uart_puts("Hard Fault \r\n");
#endif

	while(1);
}

/* Initialize the interrupts for the physical sensors */
void PhysSensors_IntInit(void)
{
	extern signed char dev_i2c_write(unsigned char dev_addr, unsigned char reg_addr, unsigned char *reg_data, unsigned char cnt);
    uint8_t dat;

	dat = 0x55;
	dev_i2c_write(ADDR_BMI160, 0x53, &dat, 1);
	dat = 0x02;
	dev_i2c_write(ADDR_BMC150A, 0x20, &dat, 1);
	dat = 0x0F;
	dev_i2c_write(ADDR_BMI055G, 0x16, &dat, 1);

    dat = 0x0A; /* Configure IRQ as open drain */
	dev_i2c_write(ADDR_BMI055, 0x20, &dat, 1);

	dat = 0x07;
	dev_i2c_write(ADDR_BMM150, 0x4E, &dat, 1);
	dat = 0x07;
	dev_i2c_write(ADDR_BMC150A, 0x4E, &dat, 1);
}

#define CPUCTRL_ADDR       0x40000300
#define READ_CPUCTRL()     (*(volatile uint32_t *)CPUCTRL_ADDR)
#define WRITE_CPUCTRL(val) ((*(volatile uint32_t *)CPUCTRL_ADDR) = (val))

#define DEV_REG (*(unsigned int *)0x40000300)
int main(void) {
	uint32_t sleep_ok = 0;
	uint32_t ind = 0;
	uint32_t lastPrintTime, CurrTime;
	volatile uint32_t pressure;
	WRITE_CPUCTRL(0xC0C4000D);
	CurrTime = READ_CPUCTRL();

	myCoreBox = MAILBOX_CM0PLUS;
	otherCoreBox = MAILBOX_CM4;


	for (ind=0; ind< 1000000; ind++){

	}

	// Set the LED to the state of "On"
	Board_LED_Set(1, false);

	/* Update core clock variables */
	SystemCoreClockUpdate();
//	/* Initialize the pinmux and clocking per board connections */
	//    	Board_Init();
#ifdef DEBUG_PRINTF
	debug_uart_init();
	uart_puts("\r\n USART Initialized, M0 reads sensors\r\n");
#endif
	/* First thing get the IRC configured and turn the PLL off */
//	setupClocking();

	/* Initialize GPIO pin interrupt module */
	Chip_PININT_Init(LPC_PININT);

	/* Initialize kernel */
	Kernel_Init();

	/* Init I2C */
	dev_i2c_init();
	/* Configure all IRQ's as tri-state */
	PhysSensors_IntInit();
	/* Initialize sensors */
	PhysSensors_Init();

	SystemCoreClockUpdate();

//	NVIC_EnableIRQ(MAILBOX_IRQn);


	// Force the counter to be placed into memory
	volatile static int i = 0 ;
	PhysSensors_Enable(PHYS_ACCEL_ID,1);
#if PROCESSING_MODE == NORMAL_PROCESSING_MODE
	PhysSensors_Enable(PHYS_GYRO_ID,0);
	PhysSensors_Enable(PHYS_MAG_ID,0);
#endif
//	PhysSensors_Enable(PHYS_PRESSURE_ID,1);
	char dataStr[80];
	Board_LED_Set(0, false);
	// Enter an infinite loop, just incrementing a counter
	while(1) {
		i++;
		sleep_ok = Sensor_process();
		//		sleep_ok |= Hostif_SleepOk();
		if (sleep_ok == 0) {
//			inactivePeriod = ResMgr_EstimateSleepTime();
//
//			CurrTime = g_Timer.GetCurrentMsec();
	#ifdef DEBUG_PRINTF
				if(CurrTime - lastPrintTime > 500){
					pressure = PysSensStatus[3].data16[1];
					pressure = pressure << 16 + PysSensStatus[3].data16[0] ;
//					pressure = (((uint32_t) PysSensStatus[3].data16[1]) << 16) + PysSensStatus[3].data16[0] ;
					lastPrintTime = g_Timer.GetCurrentMsec();
					sprintf(dataStr,"Acc: %d %d %d - Gyro: %d %d %d - Mag: %d %d %d  -  Press: %d  - Temp %d\r\n", \
							PysSensStatus[0].data16[0],PysSensStatus[0].data16[1],PysSensStatus[0].data16[2], \
							PysSensStatus[1].data16[0],PysSensStatus[1].data16[1],PysSensStatus[1].data16[2], \
							PysSensStatus[2].data16[0],PysSensStatus[2].data16[1],PysSensStatus[2].data16[2], \
							pressure,PysSensStatus[3].data16[2]);
					uart_puts(dataStr);
					dataReady =0;
				}
	#endif
		 inactivePeriod= 5000;
		 ResMgr_EnterPowerDownMode(inactivePeriod);
		}

	}
	return 0 ;
}
