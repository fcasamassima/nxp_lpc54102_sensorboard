/*
 * GlobalVars.h
 *
 *  Created on: Jul 8, 2015
 *      Author: Filippo
 */

#include "global_types.h"

#ifndef GLOBALVARS_H_
#define GLOBALVARS_H_


extern SensorStatus_t PysSensStatus[6];
extern PhysSensorCtrl_1_t PhysSensCtrl[6];
extern KernelTimer_Ctrl_t g_Timer;

static MBOX_IDX_T myCoreBox, otherCoreBox;

//SensorStatus_t Accelerometer;
//
//
//PhysSensorCtrl_1_t g_accelCtrl;


//SensorStatus_t *g_phySensors[PHYS_MAX_ID] = {
//		&Accelerometer,
//		&Accelerometer,
//		&Accelerometer,
//		&Accelerometer,
//		&Accelerometer,
//		&Accelerometer
//};

//
//const PhysicalSensor_t AccelSensor= {
//	&g_accelCtrl,       /* Sensor hardware structure */
//	{0, },              /* Sensor data sample */
//	0,                  /* Time (watchdog timer ticks) the last sample was acquired */
//	0,                  /* Time (watchdog timer ticks) the next sample will be taken */
//	PHYS_ACCEL_ID,      /* Sensor ID */
//	0,                  /* Sensor enabled status*/
//	0,                  /* IRQ pending */
//	PHYS_MODE_IRQ,      /* Data acquisition mode (irq, polled) */
//	0,                  /* Sample period in mSec */
//};
//
///* Physical sensors array */
//PhysicalSensor_t *g_phySensors[PHYS_MAX_ID] = {
//	&AccelSensor,
//	&AccelSensor,
//	&AccelSensor,
//	&AccelSensor,
//	&AccelSensor,
//	&AccelSensor,
//};

#endif /* GLOBALVARS_H_ */
