/*
 * featuresExtract.c
 *
 *  Created on: Jul 16, 2015
 *      Author: filippo
 */


#include "featuresExtract.h"
#include "arm_math.h"
#include "arm_const_structs.h"

data_t * SelectDataPtr(int axis, int sensor);

data_t ComputeMean(axesName_t axis, PhysSensorId_t sensor){
	data_t result;
	data_t* buffPtr;
	buffPtr = SelectDataPtr(axis, sensor);
	arm_mean_q15(buffPtr,BUFFER_SIZE,&result);
	return result;
}


data_t ComputeStdDev(axesName_t axis, PhysSensorId_t sensor){
	data_t result;
	data_t* buffPtr;
	float DataCopy[BUFFER_SIZE], floatRes;
	buffPtr = SelectDataPtr(axis, sensor);

/*   				WARNING!!!
 * Due to a bug in CMSIS the function"arm_std_q15(buffPtr,BUFFER_SIZE,&result)"
 * does not give proper results for this reason I'm using the float version
 *
 *
 */
    arm_q15_to_float (buffPtr, DataCopy, BUFFER_SIZE);
    arm_std_f32(DataCopy,BUFFER_SIZE,&floatRes);
    result = (int) (floatRes*32768);
	return result;
}

data_t ComputeMax(axesName_t axis, PhysSensorId_t sensor){
	data_t result;
	data_t* buffPtr;
	unsigned int index;
	buffPtr = SelectDataPtr(axis, sensor);
	arm_max_q15(buffPtr,BUFFER_SIZE,&result,&index);
	return result;
}

data_t ComputeMin(axesName_t axis, PhysSensorId_t sensor){
	data_t result;
	data_t* buffPtr;
	unsigned int index;
	buffPtr = SelectDataPtr(axis, sensor);
	arm_min_q15(buffPtr,BUFFER_SIZE,&result,&index);
	return result;
}

data_t ComputeFFT(axesName_t axis, PhysSensorId_t sensor){
	uint32_t fftSize = BUFFER_SIZE/2;
	uint32_t ifftFlag = 0,testIndex;
	uint32_t doBitReverse = 1;
	data_t fftOutput[BUFFER_SIZE/2], inputBuffer[BUFFER_SIZE];
	data_t* buffPtr;
	data_t maxValue;
	buffPtr = SelectDataPtr(axis, sensor);

	arm_copy_q15 (buffPtr, inputBuffer, BUFFER_SIZE);


	/* Process the data through the CFFT/CIFFT module */
	arm_cfft_q15(&arm_cfft_sR_q15_len64, inputBuffer, ifftFlag, doBitReverse);

	/* Process the data through the Complex Magnitude Module for
	  calculating the magnitude at each bin */
	arm_cmplx_mag_q15(inputBuffer, fftOutput, fftSize);

	/* Calculates maxValue and returns corresponding BIN value */
	arm_max_q15(fftOutput, fftSize, &maxValue, &testIndex);

	return testIndex;
}

data_t ComputeZCR(axesName_t axis, PhysSensorId_t sensor){
	uint32_t i;
	data_t counter =0;
	data_t* buffPtr;

	buffPtr = SelectDataPtr(axis, sensor);

	for (i =0; i< BUFFER_SIZE -1; i++){
		if ((buffPtr[i]&0x8000) != (buffPtr[i+1]&0x8000))
			counter++;
	}

	return counter;
}

data_t * SelectDataPtr(int axis, int sensor){
	databuff_t* sensBuff;
	switch (sensor) {
	case PHYS_ACCEL_ID:
		sensBuff = &Acc;
		break;
	case PHYS_GYRO_ID:
		sensBuff = &Gyr;
		break;
	case PHYS_MAG_ID:
		sensBuff = &Mag;
		break;
	case PHYS_PRESSURE_ID:
		sensBuff = &Press;
		break;
	default:
		break;
	}
	switch (axis) {
	case X:
		return sensBuff->X;
		break;
	case Y:
		return sensBuff->Y;
		break;
	case Z:
		return sensBuff->Z;
		break;
	default:
		return NULL;
		break;
	}
}
