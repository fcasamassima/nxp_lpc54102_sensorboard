/*
===============================================================================
 Name        : SensorInterface_Multicore_M4.c
 Author      : $(author)
 Version     :
 Copyright   : $(copyright)
 Description : main definition
===============================================================================
*/

#if defined (__USE_LPCOPEN)
#if defined(NO_BOARD_LIB)
#include "chip.h"
#else
#include "board.h"
#endif
#endif

#include <cr_section_macros.h>

#if defined (__MULTICORE_MASTER_SLAVE_M0SLAVE) || \
    defined (__MULTICORE_MASTER_SLAVE_M4SLAVE)
#include "boot_multicore_slave.h"
#endif

/* UART Driver context memory */
#define RAMBLOCK_H          60
static uint32_t  drv_mem[RAMBLOCK_H];

/* UART ROM Driver Handle */
static UART_HANDLE_T *hUART;

#define UART_BAUD_RATE     115200	/* Required UART Baud rate */
#define UART_BUAD_ERR      1/* Percentage of Error allowed in baud */

#define tmsg1(x) "Type " # x " chars to echo it back\r\n"
#define tmsg(x) tmsg1(x)
#define RX_SZ  16
//#define msg   tmsg(RX_SZ)
#define msg "Tree computation completed \r\n"
#define rmsg  "UART received : "

static MBOX_IDX_T myCoreBox, otherCoreBox;
/*****************************************************************************
 * Private functions
 ****************************************************************************/

static void UART_PinMuxSetup(void)
{
#if defined(BOARD_NXP_LPCXPRESSO_54102)
	/* Setup UART TX Pin */
	Chip_IOCON_PinMuxSet(LPC_IOCON, 0, 0, (IOCON_FUNC1 | IOCON_MODE_INACT | IOCON_DIGITAL_EN));
	/* Setup UART RX Pin */
	Chip_IOCON_PinMuxSet(LPC_IOCON, 0, 1, (IOCON_FUNC1 | IOCON_MODE_INACT | IOCON_DIGITAL_EN));
	Chip_SYSCON_Enable_ASYNC_Syscon(true);	/* Enable Async APB */
	Chip_Clock_SetAsyncSysconClockDiv(1);	/* Set Async clock divider to 1 */
#else
#warning "No UART PIN/CLK setup for this example"
#endif
}


#define ABS(x) ((int) (x) < 0 ? -(x) : (x))


void MAILBOX_IRQHandler(void)
{
	/* Clear this MCU's mailbox */
	Chip_MBOX_ClearValueBits(LPC_MBOX, myCoreBox, 0xFFFFFFFF);
}

static void setupClocking(void)
{
	/* Set main clock source to the IRC clock  This will drive 12MHz
	   for the main clock and 12MHz for the system clock */
	Chip_Clock_SetMainClockSource(SYSCON_MAINCLKSRC_IRC);

	/* Make sure the PLL is off */
	Chip_SYSCON_PowerDown(SYSCON_PDRUNCFG_PD_SYS_PLL);

    /* Wait State setting TBD */
	/* Setup FLASH access to 2 clocks (up to 20MHz) */
    Chip_SYSCON_SetFLASHAccess(FLASHTIM_20MHZ_CPU);

	/* Set system clock divider to 1 */
	Chip_Clock_SetSysClockDiv(1);

    /* ASYSNC SYSCON needs to be on or all serial peripheral won't work.
	   Be careful if PLL is used or not, ASYNC_SYSCON source needs to be
	   selected carefully. */
	Chip_SYSCON_Enable_ASYNC_Syscon(true);
	Chip_Clock_SetAsyncSysconClockDiv(1);
	Chip_Clock_SetAsyncSysconClockSource(SYSCON_ASYNC_IRC);
}


// TODO: insert other definitions and declarations here
#define CPUCTRL_ADDR       0x40000300
#define READ_CPUCTRL()     (*(volatile uint32_t *)CPUCTRL_ADDR)
#define WRITE_CPUCTRL(val) ((*(volatile uint32_t *)CPUCTRL_ADDR) = (val))


int main(void) {
	uint32_t cpuctrl;

#if defined (__USE_LPCOPEN)
    // Read clock settings and update SystemCoreClock variable
    SystemCoreClockUpdate();
#if !defined(NO_BOARD_LIB)
#if defined (__MULTICORE_MASTER) || defined (__MULTICORE_NONE)
    // Set up and initialize all required blocks and
    // functions related to the board hardware
    Board_Init();
    setupClocking();
#endif
    // Set the LED to the state of "On"
    Board_LED_Set(1, true);
#endif
#endif

	/* Get the mailbox identifiers to the core this code is running and the
	   other core */
	myCoreBox = MAILBOX_CM4;
	otherCoreBox = MAILBOX_CM0PLUS;

	//Give M0 control of LP modes
	(*(volatile uint32_t *)CPUCTRL_ADDR) = 0xC0C4000D;

#if defined (__MULTICORE_MASTER_SLAVE_M0SLAVE) || \
    defined (__MULTICORE_MASTER_SLAVE_M4SLAVE)
    boot_multicore_slave();
#endif


    while(1) {
    	__WFI();
    }
    return 0 ;
}
