# TruConnectAndroid
Android library for ACKme TruConnect BLE devices

# Installation
This project should be imported into Android studio (File -> Import Project).  

# Application Note
For an application example, please see http://truconnect.ack.me/1.5/apps/truconnect_android_library

# Usage
The demo app uses a Service to allow multiple activities to share a BLE connection.  The activities 
receive intents from the Service as BLE events happen.

The basic usage of the library is as follows:
```
mTruconnectManager = new TruconnectManager();
mTruconnectManager.init(context, mCallbacks);

mTruconnectManager.startScan();

mTruconnectManager.connect("device_name");

mTruconnectManager.GPIOFunctionSet(LED_GPIO, TruconnectGPIOFunction.STDIO);
mTruconnectManager.GPIODirectionSet(LED_GPIO, TruconnectGPIODirection.OUTPUT_LOW);
mTruconnectManager.GPIOSet(14, 1);//set GPIO14 high to turn LED on

mTruconnectManager.adc(10);//read ADC value on GPIO10

```

The above functions add commands into a queue.  The results are accessed through the callbacks 
passed into init().

Please see the demo app for a full implementation of the library.

# License

Copyright (C) 2015, Sensors.com,  Inc. All Rights Reserved.

The TruConnect Android Library and TruConnect example applications are provided free of charge by
Sensors.com. The combined source code, and all derivatives, are licensed by Sensors.com SOLELY
for use with devices manufactured by ACKme Networks, or devices approved by Sensors.com.

Use of this software on any other devices or hardware platforms is strictly prohibited.

THIS SOFTWARE IS PROVIDED BY THE AUTHOR AS IS AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING,
BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

# About
This library makes use of the `SmoothProgressBar` library from castorflex, which uses an Apache 2.0 license.

Copyright 2014 Antoine Merle

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
