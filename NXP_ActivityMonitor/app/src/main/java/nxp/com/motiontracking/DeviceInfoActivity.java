/* TruConnect Android Library & Example Applications
*
* Copyright (C) 2015, Sensors.com,  Inc. All Rights Reserved.
*
* The TruConnect Android Library and TruConnect example applications are provided free of charge by
* Sensors.com. The combined source code, and all derivatives, are licensed by Sensors.com SOLELY
* for use with devices manufactured by ACKme Networks, or devices approved by Sensors.com.
*
* Use of this software on any other devices or hardware platforms is strictly prohibited.
*
* THIS SOFTWARE IS PROVIDED BY THE AUTHOR AS IS AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING,
* BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
* PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
* INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
* PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
* LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
* SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

package nxp.com.motiontracking;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;


import ack.me.truconnectandroid.OTA.FirmwareVersion;
import ack.me.truconnectandroid.OTA.OTACallbacks;
import ack.me.truconnectandroid.OTA.OTAStatus;
import ack.me.truconnectandroid.truconnect.TruconnectCommand;
import ack.me.truconnectandroid.truconnect.TruconnectErrorCode;
import ack.me.truconnectandroid.truconnect.TruconnectGPIODirection;
import ack.me.truconnectandroid.truconnect.TruconnectGPIOFunction;
import ack.me.truconnectandroid.truconnect.TruconnectManager;
import ack.me.truconnectandroid.truconnect.TruconnectResult;
import nxp.com.truconnectandroiddemo.R;

public class DeviceInfoActivity extends Activity
{
    public static final String TAG = "DeviceInfo";

    private final long DISCONNECT_TIMEOUT_MS = 10000;

    private static final int ADC_GPIO = 12;//thermistor on wahoo
    private static final int TEST_GPIO = 9;//button2 on wahoo
    private static final int LED_GPIO = 14;

//    private TextView mADCTextView;
//    private TextView mGPIOTextView;
//    private Button mUpdateButton;
//    private ToggleButton mLedButton;
    private int mLedState = 0;

//    private ToggleButton mModeButton;
    private int mCurrentMode;

    private Button mSendTextButton;
    private EditText mTextToSendBox;

    private TextView mReceivedDataTextBox;

    private ServiceConnection mConnection;
    private TruconnectService mService;
    private boolean mBound = false;

    private LocalBroadcastManager mLocalBroadcastManager;
    private BroadcastReceiver mBroadcastReceiver;
    private IntentFilter mReceiverIntentFilter;

    private TruconnectManager mTruconnectManager;

    private ProgressDialog mDisconnectDialog;

    private boolean mDisconnecting = false;

    private Handler mHandler;
    private Runnable mDisconnectTimeoutTask;

    private int mCurrentADCCommandID;
    private int mCurrentGPIOCommandID;

    private ImageView actImage;

    private byte[] mOTAFileData;

    private OTACallbacks mOTACallbacks = new OTACallbacks()
    {
        @Override
        public void onUpdateInitSuccess(String deviceName, FirmwareVersion currentVersion)
        {
            Log.d(TAG, "OTA init success");
        }

        @Override
        public void onUpdateCheckComplete(String deviceName, boolean isUpToDate, FirmwareVersion version, byte[] file)
        {
            if (isUpToDate)
            {
                Log.d(TAG, "Firmware already up to date!");
            }
            else
            {
                Log.d(TAG, "New update available! New version: " + version);
                mOTAFileData = file;
            }
        }

        @Override
        public void onUpdateAbort(String deviceName)
        {
            Log.d(TAG, "OTA aborted");
        }

        @Override
        public void onUpdateStart(String deviceName)
        {
            Log.d(TAG, "OTA started");
        }

        @Override
        public void onUpdateComplete(String deviceName)
        {
            Log.d(TAG, "OTA completed successfully!");
        }

        @Override
        public void onUpdateDataSent(String deviceName, int bytesSent, int bytesRemaining)
        {
            Log.d(TAG, bytesSent + " bytes sent, " + bytesRemaining + " bytes remaining");
        }

        @Override
        public void onUpdateError(String deviceName, OTAStatus status)
        {
            Log.d(TAG, "ERROR - " + status);
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_device_info);

//        mADCTextView = (TextView)findViewById(R.id.adc_value);
//        mGPIOTextView = (TextView)findViewById(R.id.gpio_value);
//        mLedButton = (ToggleButton)findViewById(R.id.led_button);

//        mLedButton.setOnClickListener(new View.OnClickListener()
//        {
//            @Override
//            public void onClick(View v)
//            {
//                if (mLedButton.isChecked())
//                {
//                    mLedState = 1;
//                    writeLedState();
//                }
//                else
//                {
//                    mLedState = 0;
//                    writeLedState();
//                }
//            }
//        });

//        mUpdateButton = (Button) findViewById(R.id.update_button);
//        mUpdateButton.setOnClickListener(new View.OnClickListener()
//        {
//            @Override
//            public void onClick(View v)
//            {
//                updateValues();
//            }
//        });

//        mModeButton = (ToggleButton) findViewById(R.id.modeButton);
//        mModeButton.setOnClickListener(new View.OnClickListener()
//        {
//            @Override
//            public void onClick(View v)
//            {
//                if (mModeButton.isChecked())
//                {
//                    mCurrentMode = TruconnectManager.MODE_STREAM;
//                    mTruconnectManager.setMode(mCurrentMode);
//                }
//                else
//                {
//                    mCurrentMode = TruconnectManager.MODE_COMMAND_REMOTE;
//                    mTruconnectManager.setMode(mCurrentMode);
//                    mTruconnectManager.setSystemCommandMode(TruconnectCommandMode.MACHINE);
//                    initGPIOs();
//                    updateValues();
//                }
//
//            }
//        });

        actImage = (ImageView)findViewById(R.id.imageActivity);

        mTextToSendBox = (EditText) findViewById(R.id.textToSend);
        mSendTextButton = (Button) findViewById(R.id.sendTextButton);
        mSendTextButton.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                String data = mTextToSendBox.getText().toString();
                if (data != null && !data.isEmpty())
                {
                    mTruconnectManager.writeData(data);
                }

                mTextToSendBox.setText("");//clear input after send
            }
        });

        mReceivedDataTextBox = (TextView) findViewById(R.id.receivedDataBox);

        initBroadcastManager();
        initBroadcastReceiver();
        initServiceConnection();
        initReceiverIntentFilter();

        GUISetStreamMode();
//        mModeButton.toggle();//start on (in stream mode)

        mHandler = new Handler();

        mDisconnectTimeoutTask = new Runnable()
        {
            @Override
            public void run()
            {
                dismissProgressDialog();
                showErrorDialog(R.string.error, R.string.discon_timeout_message);
            }
        };
    }

    @Override
    public void onBackPressed()
    {
        disconnect();
    }

    @Override
    protected void onStart()
    {
        super.onStart();

        mLocalBroadcastManager.registerReceiver(mBroadcastReceiver, mReceiverIntentFilter);

        Intent serviceIntent = new Intent(getApplicationContext(), TruconnectService.class);
        bindService(serviceIntent, mConnection, Context.BIND_AUTO_CREATE);
    }

    @Override
    protected void onStop()
    {
        super.onStop();
        mLocalBroadcastManager.unregisterReceiver(mBroadcastReceiver);

        if (mBound)
        {
            unbindService(mConnection);
        }

        cancelDisconnectTimeout();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu)
    {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_device_info, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        // Handle item selection
        switch (item.getItemId())
        {
            case R.id.action_about:
                openAboutDialog();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void initServiceConnection()
    {
        mConnection = new ServiceConnection()
        {
            @Override
            public void onServiceConnected(ComponentName className, IBinder service)
            {
                TruconnectService.LocalBinder binder = (TruconnectService.LocalBinder) service;
                mService = binder.getService();
                mBound = true;

                mTruconnectManager = mService.getManager();
                mTruconnectManager.setMode(TruconnectManager.MODE_STREAM);
                mCurrentMode = TruconnectManager.MODE_STREAM;
            }

            @Override
            public void onServiceDisconnected(ComponentName arg0)
            {
                mBound = false;
            }
        };
    }

    private void initBroadcastReceiver()
    {
        mBroadcastReceiver = new BroadcastReceiver()
        {
            @Override
            public void onReceive(Context context, Intent intent)
            {
                // Get extra data included in the Intent
                String action = intent.getAction();

                switch (action)
                {
                    case TruconnectService.ACTION_COMMAND_SENT:
                        String command = TruconnectService.getCommand(intent).toString();
                        Log.d(TAG, "Command " + command + " sent");
                        break;

                    case TruconnectService.ACTION_COMMAND_RESULT:
                        handleCommandResponse(intent);
                        break;

                    case TruconnectService.ACTION_MODE_WRITE:
                        int mode = TruconnectService.getMode(intent);
                        if (mode == TruconnectManager.MODE_STREAM)
                        {
                            //disable buttons while in stream mode (must be in rem command to work)
                            GUISetStreamMode();
                        }
                        else
                        {
                            GUISetCommandMode();
                        }
                        break;

                    case TruconnectService.ACTION_STRING_DATA_READ:
                        if (mCurrentMode == TruconnectManager.MODE_STREAM)
                        {
                            String text = TruconnectService.getData(intent);
                            updateReceivedTextBox(text);
                        }
                        break;

                    case TruconnectService.ACTION_BINARY_DATA_READ:
                        //add code here to handle the received binary data
                        break;

                    case TruconnectService.ACTION_ERROR:
                        TruconnectErrorCode errorCode = TruconnectService.getErrorCode(intent);
                        //handle errors
                        switch (errorCode)
                        {
                            case DEVICE_ERROR:
                                //connection state change without request
                                handle_disconnect();
                                showErrorDialog(R.string.error, R.string.device_error_message);
                                break;

                            case DISCONNECT_FAILED:
                                handle_disconnect();
                                showDisconnectErrorDialog(R.string.error, R.string.discon_err_message);
                                break;

                            case CONNECTION_LOST:
                                handle_disconnect();
                                showErrorDialog(R.string.error, R.string.error_connection_lost);
                                break;
                        }
                        break;

                    case TruconnectService.ACTION_DISCONNECTED:
                        cancelDisconnectTimeout();
                        dismissProgressDialog();
                        finish();
                        break;
                }
            }
        };
    }

    public void initBroadcastManager()
    {
        mLocalBroadcastManager = LocalBroadcastManager.getInstance(DeviceInfoActivity.this);
    }

    public void initReceiverIntentFilter()
    {
        mReceiverIntentFilter = new IntentFilter();
        mReceiverIntentFilter.addAction(TruconnectService.ACTION_DISCONNECTED);
        mReceiverIntentFilter.addAction(TruconnectService.ACTION_COMMAND_RESULT);
        mReceiverIntentFilter.addAction(TruconnectService.ACTION_ERROR);
        mReceiverIntentFilter.addAction(TruconnectService.ACTION_STRING_DATA_READ);
        mReceiverIntentFilter.addAction(TruconnectService.ACTION_BINARY_DATA_READ);
        mReceiverIntentFilter.addAction(TruconnectService.ACTION_MODE_WRITE);
    }

    private void initGPIOs()
    {
        mTruconnectManager.GPIOFunctionSet(ADC_GPIO, TruconnectGPIOFunction.NONE);
        mTruconnectManager.GPIOFunctionSet(TEST_GPIO, TruconnectGPIOFunction.NONE);
        mTruconnectManager.GPIOFunctionSet(LED_GPIO, TruconnectGPIOFunction.NONE);

        mTruconnectManager.GPIOFunctionSet(TEST_GPIO, TruconnectGPIOFunction.STDIO);
        mTruconnectManager.GPIOFunctionSet(LED_GPIO, TruconnectGPIOFunction.STDIO);

        mTruconnectManager.GPIODirectionSet(TEST_GPIO, TruconnectGPIODirection.INPUT);
        mTruconnectManager.GPIODirectionSet(LED_GPIO, TruconnectGPIODirection.OUTPUT_LOW);
    }

    private void updateValues()
    {
        Log.d(TAG, "Updating values");
        mCurrentADCCommandID = mTruconnectManager.adc(ADC_GPIO);
        mCurrentGPIOCommandID = mTruconnectManager.GPIOGet(TEST_GPIO);
    }

    private void handleCommandResponse(Intent intent)
    {
        TruconnectCommand command = TruconnectService.getCommand(intent);
        int ID = TruconnectService.getID(intent);
        int code = TruconnectService.getResponseCode(intent);
        String result = TruconnectService.getData(intent);
        String message = "";

        Log.d(TAG, "Command " + command + " result");

        if (code == TruconnectResult.SUCCESS)
        {
            switch (command)
            {
                case ADC:
                    if (ID == mCurrentADCCommandID)
                    {
                        message = String.format("ADC: %s", result);
//                        mADCTextView.setText(message);
                    }
                    else
                    {
                        Log.d(TAG, "Invalid ADC command ID!");
                    }
                    break;

                case GPIO_GET:
                    if (ID == mCurrentGPIOCommandID)
                    {
                        message = String.format("GPIO: %s", result);
//                        mGPIOTextView.setText(message);
                    }
                    else
                    {
                        Log.d(TAG, "Invalid GPIO command ID!");
                    }
                    break;

                case GPIO_SET:
                    break;
            }
        }
        else
        {
            message = String.format("ERROR %d - %s", code, result);
            showToast(message, Toast.LENGTH_SHORT);
        }
    }

    //set up gui elements for command mode operation
    private void GUISetCommandMode()
    {
//        mLedButton.setEnabled(true);
//        mUpdateButton.setEnabled(true);
        mSendTextButton.setEnabled(false);
        mTextToSendBox.setVisibility(View.INVISIBLE);
    }

    //set up gui elements for command mode operation
    private void GUISetStreamMode()
    {
//        mLedButton.setEnabled(false);
//        mUpdateButton.setEnabled(false);
        mSendTextButton.setEnabled(true);
        mTextToSendBox.setVisibility(View.VISIBLE);
    }

    private void disconnect()
    {
        mDisconnecting = true;
        showDisconnectDialog();
        mTruconnectManager.disconnect(true);
        mHandler.postDelayed(mDisconnectTimeoutTask, DISCONNECT_TIMEOUT_MS);
    }
    int dataCounter = 0;
    private String RxData = "";
    private void updateReceivedTextBox(String newData)
    {
        Boolean found;
        RxData = RxData + newData;
        found = RxData.contains("\r\n");
        if (found) {
            mReceivedDataTextBox.setText(Integer.toString(dataCounter++) + " " + RxData );
            if (RxData.contains("Wal")){
                actImage.setImageResource(R.drawable.act_walk);
            }
            if (RxData.contains("Run")){
                actImage.setImageResource(R.drawable.act_run);
            }
            if (RxData.contains("Sea")){
                actImage.setImageResource(R.drawable.act_seat);
            }
            if (RxData.contains("Sta")){
                actImage.setImageResource(R.drawable.act_stand);
            }
            if (RxData.contains("Unk")){
                actImage.setImageResource(R.drawable.act_unknown);
            }
            if (RxData.contains("Lay")){
                actImage.setImageResource(R.drawable.act_lie);
            }
            if (RxData.contains("Bik")){
                actImage.setImageResource(R.drawable.act_bike);
            }

            RxData = "";
        }

    }

    private void clearReceivedTextBox()
    {
        mReceivedDataTextBox.setText("");
    }

    private void showDisconnectDialog()
    {
        final ProgressDialog dialog = new ProgressDialog(DeviceInfoActivity.this);
        String title = getString(R.string.disconnect_dialog_title);
        String msg = getString(R.string.disconnect_dialog_message);
        dialog.setIndeterminate(true);//Dont know how long disconnect could take.....
        dialog.setCancelable(false);

        mDisconnectDialog = dialog.show(DeviceInfoActivity.this, title, msg);
        mDisconnectDialog.setCancelable(false);
    }

    private void showDisconnectErrorDialog(final int titleID, final int msgID)
    {
        AlertDialog.Builder builder = new AlertDialog.Builder(DeviceInfoActivity.this);

        builder.setTitle(titleID)
                .setMessage(msgID)
                .setPositiveButton(R.string.retry, new DialogInterface.OnClickListener()
                {
                    @Override
                    public void onClick(DialogInterface dialog, int which)
                    {
                        dialog.dismiss();
                        disconnect();
                    }
                })
                .setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener()
                {
                    @Override
                    public void onClick(DialogInterface dialog, int which)
                    {
                        dialog.dismiss();
                        DeviceInfoActivity.this.finish();
                    }
                })
                .create()
                .show();
    }

    private void showErrorDialog(final int titleID, final int msgID)
    {
        AlertDialog.Builder builder = new AlertDialog.Builder(DeviceInfoActivity.this);

        builder.setTitle(titleID)
                .setMessage(msgID)
                .setNegativeButton(R.string.ok, new DialogInterface.OnClickListener()
                {
                    @Override
                    public void onClick(DialogInterface dialog, int which)
                    {
                        dialog.dismiss();
                        DeviceInfoActivity.this.finish();
                    }
                })
                .create()
                .show();
    }

    private void showToast(final String msg, final int duration)
    {
        runOnUiThread(new Runnable()
        {
            @Override
            public void run()
            {
                Toast.makeText(getApplicationContext(), msg, duration).show();
            }
        });
    }

    private void dismissProgressDialog()
    {
        if (mDisconnectDialog != null)
        {
            mDisconnectDialog.dismiss();
        }
    }

    private void writeLedState()
    {
        mTruconnectManager.GPIOSet(LED_GPIO, mLedState);
    }

    private void cancelDisconnectTimeout()
    {
        mHandler.removeCallbacks(mDisconnectTimeoutTask);
    }

    private void openAboutDialog()
    {
        runOnUiThread(new Runnable()
        {
            @Override
            public void run()
            {
                Dialogs.makeAboutDialog(DeviceInfoActivity.this).show();
            }
        });
    }

    private void handle_disconnect()
    {
        mDisconnecting = false;
        dismissProgressDialog();
    }
}
