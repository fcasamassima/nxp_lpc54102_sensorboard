/* TruConnect Android Library & Example Applications
*
* Copyright (C) 2015, Sensors.com,  Inc. All Rights Reserved.
*
* The TruConnect Android Library and TruConnect example applications are provided free of charge by
* Sensors.com. The combined source code, and all derivatives, are licensed by Sensors.com SOLELY
* for use with devices manufactured by ACKme Networks, or devices approved by Sensors.com.
*
* Use of this software on any other devices or hardware platforms is strictly prohibited.
*
* THIS SOFTWARE IS PROVIDED BY THE AUTHOR AS IS AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING,
* BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
* PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
* INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
* PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
* LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
* SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

package nxp.com.motiontracking;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.pm.PackageManager;
import android.view.Gravity;
import android.widget.TextView;

import nxp.com.truconnectandroiddemo.R;

public class Dialogs
{
    public static AlertDialog makeAboutDialog(Activity activity)
    {
        AlertDialog dialog = new AlertDialog.Builder(activity)
               .setTitle(R.string.about)
               .setCancelable(false)
               .setPositiveButton(R.string.ok, new DialogInterface.OnClickListener()
               {
                   public void onClick(DialogInterface dialog, int id)
                   {
                       dialog.dismiss();
                   }
               })
               .create();

        return setAboutDialogContents(activity, dialog);
    }

    private static AlertDialog setAboutDialogContents(Activity activity, AlertDialog dialog)
    {
        String app_name = activity.getString(R.string.app_name);
        String link = activity.getString(R.string.about_link);
        String version = getAppVersion(activity.getApplicationContext());

        String msg = String.format("%s\nv%s\n%s", app_name, version, link);

        TextView messageView = new TextView(activity);
        messageView.setText(msg);
        messageView.setGravity(Gravity.CENTER_HORIZONTAL);
        messageView.setTextSize(20.0f);
        dialog.setView(messageView);

        return dialog;
    }

    private static String getAppVersion(Context context)
    {
        PackageManager packageManager = context.getPackageManager();
        String packageName = context.getPackageName();

        String versionName = ""; // initialize String

        if (packageManager != null && packageName != null)
        {
            try
            {
                versionName = packageManager.getPackageInfo(packageName, 0).versionName;
            }
            catch (PackageManager.NameNotFoundException e)
            {
            }
        }

        return versionName;
    }

}
