/*
===============================================================================
 Name        : SensorInterface_Multicore_M4.c
 Author      : $(author)
 Version     :
 Copyright   : $(copyright)
 Description : main definition
===============================================================================
 */
//#define DEBUG_PRINTF
#if defined (__USE_LPCOPEN)
#if defined(NO_BOARD_LIB)
#include "chip.h"
#else
#include "board.h"
#endif
#endif
#include "sensorhub.h"
#include "sensorhubBoard.h"
#include "sensors.h"
//#include "hostif.h"
#include "sensacq_i2c.h"
#include "stopwatch.h"
#include "global_types.h"
#include "GlobalVars.h"
#include "global_defines.h"
#ifdef DEBUG_PRINTF
#include "uart_polling_m4.h"
#endif
int32_t inactivePeriod = 0;
uint8_t dataReady=0;

SensorStatus_t PysSensStatus[6];
PhysSensorCtrl_1_t PhysSensCtrl[6];
KernelTimer_Ctrl_t g_Timer;

#include <cr_section_macros.h>

#if defined (__MULTICORE_MASTER_SLAVE_M0SLAVE) || \
		defined (__MULTICORE_MASTER_SLAVE_M4SLAVE)
#include "boot_multicore_slave.h"
#endif

// TODO: insert other include files here
// TODO: insert other include files here
static void setupClocking(void)
{
	/* Set main clock source to the IRC clock  This will drive 24MHz
	   for the main clock and 24MHz for the system clock */
	Chip_Clock_SetMainClockSource(SYSCON_MAINCLKSRC_IRC);

	/* Make sure the PLL is off */
	Chip_SYSCON_PowerDown(SYSCON_PDRUNCFG_PD_SYS_PLL);

	/* Wait State setting TBD */
	/* Setup FLASH access to 2 clocks (up to 20MHz) */
	Chip_SYSCON_SetFLASHAccess(FLASHTIM_20MHZ_CPU);

	/* Set system clock divider to 1 */
	Chip_Clock_SetSysClockDiv(1);

	/* ASYSNC SYSCON needs to be on or all serial peripheral won't work.
	   Be careful if PLL is used or not, ASYNC_SYSCON source needs to be
	   selected carefully. */
	Chip_SYSCON_Enable_ASYNC_Syscon(true);
	Chip_Clock_SetAsyncSysconClockDiv(1);
	Chip_Clock_SetAsyncSysconClockSource(SYSCON_ASYNC_IRC);
}

/* Initialize the interrupts for the physical sensors */
void PhysSensors_IntInit(void)
{
	extern signed char dev_i2c_write(unsigned char dev_addr, unsigned char reg_addr, unsigned char *reg_data, unsigned char cnt);
	uint8_t dat;

	dat = 0x55;
	dev_i2c_write(ADDR_BMI160, 0x53, &dat, 1);
	dat = 0x02;
	dev_i2c_write(ADDR_BMC150A, 0x20, &dat, 1);
	dat = 0x0F;
	dev_i2c_write(ADDR_BMI055G, 0x16, &dat, 1);

	dat = 0x0A; /* Configure IRQ as open drain */
	dev_i2c_write(ADDR_BMI055, 0x20, &dat, 1);

	dat = 0x07;
	dev_i2c_write(ADDR_BMM150, 0x4E, &dat, 1);
	dat = 0x07;
	dev_i2c_write(ADDR_BMC150A, 0x4E, &dat, 1);
}


// TODO: insert other definitions and declarations here

int main(void) {
	uint32_t sleep_ok = 0;
	uint32_t ind = 0;
	uint32_t CurrTime,lastPrintTime;

#if defined (__USE_LPCOPEN)
	// Read clock settings and update SystemCoreClock variable
	SystemCoreClockUpdate();
#if !defined(NO_BOARD_LIB)
#if defined (__MULTICORE_MASTER) || defined (__MULTICORE_NONE)
	// Set up and initialize all required blocks and
	// functions related to the board hardware
	Board_Init();
	setupClocking();
#endif
	// Set the LED to the state of "On"
	Board_LED_Set(1, true);
#endif
#endif

#if defined (__MULTICORE_MASTER_SLAVE_M0SLAVE) || \
		defined (__MULTICORE_MASTER_SLAVE_M4SLAVE)
	boot_multicore_slave();
#endif

	/* Initialize GPIO pin interrupt module */
	Chip_PININT_Init(LPC_PININT);

	/* Initialize kernel */
	Kernel_Init();

	/* Init I2C */
	dev_i2c_init();
	/* Configure all IRQ's as tri-state */
	PhysSensors_IntInit();
	/* Initialize sensors */
	PhysSensors_Init();

	SystemCoreClockUpdate();
//	Board_Debug_Init();
//	Board_UARTPutSTR("\r\n USART Initialized");
// define DEBUG_PRINTF
#ifdef DEBUG_PRINTF
	debug_uart_init();
	uart_puts("\r\n USART Initialized");
#endif

	// Force the counter to be placed into memory
	volatile static int i = 0 ;


	PhysSensors_Enable(PHYS_ACCEL_ID,1);
#if PROCESSING_MODE == NORMAL_MODE
	PhysSensors_Enable(PHYS_GYRO_ID,0);
	PhysSensors_Enable(PHYS_MAG_ID,0);
#endif
	char dataStr[80];
	Board_LED_Set(0, false);
	// Enter an infinite loop, just incrementing a counter
	while(1) {
	//	sleep_ok |= Hostif_SleepOk();
		sleep_ok = Sensor_process();
		if (sleep_ok == 0) {
//			inactivePeriod = ResMgr_EstimateSleepTime();

			CurrTime = g_Timer.GetCurrentMsec();
#ifdef DEBUG_PRINTF
			if(CurrTime - lastPrintTime > 500){
				lastPrintTime = g_Timer.GetCurrentMsec();
				sprintf(dataStr,"Acc: %d %d %d - Gyro: %d %d %d - Mag: %d %d %d\r\n", \
						PysSensStatus[0].data16[0],PysSensStatus[0].data16[1],PysSensStatus[0].data16[2], \
						PysSensStatus[1].data16[0],PysSensStatus[1].data16[1],PysSensStatus[1].data16[2], \
						PysSensStatus[2].data16[0],PysSensStatus[2].data16[1],PysSensStatus[2].data16[2]);
				uart_puts(dataStr);
				dataReady =0;
			}
#endif
//			ResMgr_EnterPowerDownMode(inactivePeriod);
			ResMgr_EnterPowerDownMode(5000);
		}

	}
	return 0 ;
}
