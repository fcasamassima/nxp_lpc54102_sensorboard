/*
 * @brief Always on timer interface
 *
 * @note
 * Copyright(C) NXP Semiconductors, 2013
 * All rights reserved.
 *
 * @par
 * Software that is described herein is for illustrative purposes only
 * which provides customers with programming information regarding the
 * LPC products.  This software is supplied "AS IS" without any warranties of
 * any kind, and NXP Semiconductors and its licenser disclaim any and
 * all warranties, express or implied, including all implied warranties of
 * merchantability, fitness for a particular purpose and non-infringement of
 * intellectual property rights.  NXP Semiconductors assumes no responsibility
 * or liability for the use of the software, conveys no license or rights under any
 * patent, copyright, mask work right, or any other intellectual property rights in
 * or to any products. NXP Semiconductors reserves the right to make changes
 * in the software without notification. NXP Semiconductors also makes no
 * representation or warranty that such application will be suitable for the
 * specified use without further testing or modification.
 *
 * @par
 * Permission to use, copy, modify, and distribute this software and its
 * documentation is hereby granted, under NXP Semiconductors' and its
 * licensor's relevant copyrights in the software, without fee, provided that it
 * is used in conjunction with NXP Semiconductors microcontrollers.  This
 * copyright, permission, and disclaimer notice must appear in all copies of
 * this code.
 */

#include <stdint.h>
#include "global_types.h"
#include "GlobalVars.h"
#ifndef __KERNEL_TIMER_H_
#define __KERNEL_TIMER_H_

#ifdef __cplusplus
extern "C" {
#endif

/** @defgroup KERNEL_TIMER Sensor Hub: Kernel timer interface
 * @ingroup SENSOR_HUB
 * @{
 */



//extern KernelTimer_Ctrl_t g_Timer; /**< Data structure which holds kernel timer function pointers structure */

/**
 * @brief	Initialize kernel sub-system
 * @return	none
 */
void Kernel_Init(void);

/**
 * @brief	Get current time
 * @return	Returns current 32 bit tick count
 */
static INLINE uint32_t Kernel_GetCurrentTime(void)
{
	return g_Timer.GetCurrent();
}

/**
 * @brief	Get the high byte of 40 bit timer count
 * @return	Returns high byte of 40 bit timer count
 */
static INLINE uint8_t Kernel_GetTimer40HiByte(void)
{
	return g_Timer.GetTimer40HiByte();
}

/**
 * @brief	Get kernel timer tick resolution
 * @return	Returns number of kernel tick in one second
 */
static INLINE uint32_t Kernel_GetResolution(void)
{
	return g_Timer.GetResolution();
}

/**
 * @}
 */

#ifdef __cplusplus
}
#endif

#endif /* __KERNEL_TIMER_H_ */
