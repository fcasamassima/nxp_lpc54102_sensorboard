/*
 * uart_polling_m4.h
 *
 *  Created on: Jul 13, 2015
 *      Author: filippo
 */

#ifndef UART_POLLING_M4_H_
#define UART_POLLING_M4_H_


#include "board.h"
#include "romapi_uart.h"

/** @defgroup UART_POLLING_5410X UART polling example using the ROM API
 * @ingroup EXAMPLES_ROM_5410X
 * @include "rom\uart_polling\readme.txt"
 */

/**
 * @}
 */

/*****************************************************************************
 * Private types/enumerations/variables
 ****************************************************************************/
/* UART Driver context memory */
//#define RAMBLOCK_H          60
//static uint32_t  drv_mem[RAMBLOCK_H];
//
///* UART ROM Driver Handle */
//static UART_HANDLE_T *hUART;
//
//#define UART_BAUD_RATE     115200	/* Required UART Baud rate */
//#define UART_BUAD_ERR      1/* Percentage of Error allowed in baud */
//
//#define tmsg1(x) "Type " # x " chars to echo it back\r\n"
//#define tmsg(x) tmsg1(x)
//#define RX_SZ  16
//#define msg   tmsg(RX_SZ)
//#define rmsg  "UART received : "
//
//#define DLY(x) if (1) {volatile uint32_t dl = (x); while (dl--) {}}

/*****************************************************************************
 * Public types/enumerations/variables
 ****************************************************************************/

/* Send data to UART: Blocking */
ErrorCode_t ROM_UART_SendBlock(UART_HANDLE_T hUART, const void *buffer, uint16_t size);

/* Receive data from UART: Blocking */
ErrorCode_t ROM_UART_ReceiveBlock(UART_HANDLE_T hUART, void *buffer, uint16_t size);

char *uart_gets(char *buf, int sz);

const char *uart_puts(const char *buf);

int debug_uart_init( void );


#endif /* UART_POLLING_M4_H_ */
