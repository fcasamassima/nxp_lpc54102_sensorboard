/*
 * dataBuffer.h
 *
 *  Created on: Jul 16, 2015
 *      Author: filippo
 */

#ifndef DATABUFFER_H_
#define DATABUFFER_H_

#include "sensors.h"
#include "global_defines.h"

#define ACC
#define GYRO
#define MAGNETO
#define PRESS

typedef short int data_t;

typedef enum axes {
	X,
	Y,
	Z
}axesName_t;


typedef struct DataBuffers
{
	data_t X[BUFFER_SIZE];
	data_t Y[BUFFER_SIZE];
	data_t Z[BUFFER_SIZE];
	int currIndex;

}databuff_t;



#ifdef ACC
databuff_t Acc;
int addAccMeasure(data_t NewData[3]);
#endif

#ifdef GYRO
databuff_t Gyr;
int addGyrMeasure(data_t NewData[3]);
#endif

#ifdef MAGNETO
databuff_t Mag;
int addMagMeasure(data_t NewData[3]);
#endif

#ifdef PRESS
//static int PressIndex;
databuff_t Press;
int addPressMeasure(data_t NewData);
#endif

#endif /* DATABUFFER_H_ */
