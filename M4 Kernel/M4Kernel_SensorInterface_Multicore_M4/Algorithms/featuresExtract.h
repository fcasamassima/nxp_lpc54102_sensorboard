/*
 * featuresExtract.h
 *
 *  Created on: Jul 16, 2015
 *      Author: filippo
 */

#ifndef FEATURESEXTRACT_H_
#define FEATURESEXTRACT_H_

#include "dataBuffer.h"

typedef struct  FeaturesBuffers
{
	data_t Max[3];
	data_t Min[3];
	data_t Avg[3];
	data_t Span[3];
	data_t ZCR[3];
	data_t Std[3];
	data_t SpectrPWR[3];
} features_t;

features_t AccFeat;

features_t GyrFeat;

features_t MagFeat;

features_t PressFeat;

data_t ComputeMean(axesName_t axis, PhysSensorId_t sensor);
data_t ComputeStdDev(axesName_t axis, PhysSensorId_t sensor);
data_t ComputeMax(axesName_t axis, PhysSensorId_t sensor);
data_t ComputeMin(axesName_t axis, PhysSensorId_t sensor);
data_t ComputeFFT(axesName_t axis, PhysSensorId_t sensor);

#endif /* FEATURESEXTRACT_H_ */
