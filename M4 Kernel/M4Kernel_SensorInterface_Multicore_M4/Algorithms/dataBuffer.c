/*
 * dataBuffer.c
 *
 *  Created on: Jul 16, 2015
 *      Author: filippo
 */


#include "dataBuffer.h"


#ifdef ACC
int addAccMeasure(data_t NewData[3]){
	Acc.X[Acc.currIndex] = NewData[0];
	Acc.Y[Acc.currIndex] = NewData[1];
	Acc.Z[Acc.currIndex] = NewData[2];
	Acc.currIndex++;
	if (Acc.currIndex >= BUFFER_SIZE)
		Acc.currIndex = 0;
	return Acc.currIndex;
}

#endif


#ifdef GYRO
int addGyrMeasure(data_t NewData[3]){
	Gyr.X[Gyr.currIndex] = NewData[0];
	Gyr.Y[Gyr.currIndex] = NewData[1];
	Gyr.Z[Gyr.currIndex] = NewData[2];
	Gyr.currIndex++;
	if (Gyr.currIndex >= BUFFER_SIZE)
		Gyr.currIndex = 0;
	return Gyr.currIndex;
}
#endif

//TODO add functions for other sensors
