/*
===============================================================================
 Name        : SensorInterface_Multicore_M0.c
 Author      : $(author)
 Version     :
 Copyright   : $(copyright)
 Description : main definition
===============================================================================
*/

//#define DEBUG_PRINTF
#if defined (__USE_LPCOPEN)
#if defined(NO_BOARD_LIB)
#include "chip.h"
#else
#include "board.h"
#ifdef DEBUG_PRINTF
#include "uart_polling_m0.h"
#endif

#endif
#endif

#include <cr_section_macros.h>

#if defined (__MULTICORE_MASTER_SLAVE_M0SLAVE) || \
    defined (__MULTICORE_MASTER_SLAVE_M4SLAVE)
#include "boot_multicore_slave.h"
#endif



void HardFault_Handler(void){
	Board_LED_Set(1, false);
	Board_LED_Set(0, true);
#ifdef DEBUG_PRINTF
	uart_puts("Hard Fault \r\n");
#endif

	while(1);
}



int main(void) {
	uint32_t sleep_ok = 0;
	uint32_t ind = 0;
	for (ind=0; ind< 100000; ind++){

	}

	// Set the LED to the state of "Off" just to be sure that M0 booted
	Board_LED_Set(1, false);

	/* Update core clock variables */
	SystemCoreClockUpdate();


#ifdef DEBUG_PRINTF
	debug_uart_init();
#endif
	/* First thing get the IRC configured and turn the PLL off */

	while(1){
		POWER_MODE_T powerMode  = POWER_SLEEP;
		/* Set voltage as low as possible */
		Chip_POWER_SetVoltage(POWER_BALANCED_MODE, Chip_Clock_GetMainClockRate());
		/* Now enter sleep / power down state */
		Chip_POWER_EnterPowerMode(powerMode, (SYSCON_PDRUNCFG_PD_WDT_OSC | SYSCON_PDRUNCFG_PD_SRAM0A
				| SYSCON_PDRUNCFG_PD_SRAM0B | SYSCON_PDRUNCFG_PD_SRAM1));
	}
	return 0 ;
}
