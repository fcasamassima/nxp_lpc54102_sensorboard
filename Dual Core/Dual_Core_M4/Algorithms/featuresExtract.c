/*
 * featuresExtract.c
 *
 *  Created on: Jul 16, 2015
 *      Author: filippo
 */


#include "featuresExtract.h"
#include "arm_math.h"
#include "arm_const_structs.h"


data_t ComputeMean(data_t* buffPtr){
	data_t result;
	arm_mean_q15(buffPtr,BUFFER_SIZE,&result);
	return result;
}


data_t ComputeStdDev(data_t* buffPtr){
	data_t result;
	int32_t DataCopy[BUFFER_SIZE], floatRes;

/*   				WARNING!!!
 * Due to a bug in CMSIS the function"arm_std_q15(buffPtr,BUFFER_SIZE,&result)"
 * does not give proper results for this reason I'm using the float version
 *
 *
 */
//    arm_q15_to_float (buffPtr, DataCopy, BUFFER_SIZE);
//    arm_std_f32(DataCopy,BUFFER_SIZE,&floatRes);
//    result = (int) (floatRes*32768);
	arm_q15_to_q31 (buffPtr, DataCopy, BUFFER_SIZE);
	arm_std_q31(DataCopy,BUFFER_SIZE,&floatRes);
	result = (int) (floatRes/32768);
	return result;
}

data_t ComputeMax(data_t* buffPtr){
	data_t result;
	uint32_t index;
	arm_max_q15(buffPtr,BUFFER_SIZE,&result,&index);
	return result;
}

data_t ComputeMin(data_t* buffPtr){
	data_t result;
	uint32_t index;
	arm_min_q15(buffPtr,BUFFER_SIZE,&result,&index);
	return result;
}

data_t ComputeFFT(data_t* buffPtr){
	uint32_t fftSize = BUFFER_SIZE/2;
	uint32_t ifftFlag = 0,testIndex;
	uint32_t doBitReverse = 1;
	data_t fftOutput[BUFFER_SIZE/2], inputBuffer[BUFFER_SIZE];
	data_t maxValue;

	arm_copy_q15 (buffPtr, inputBuffer, BUFFER_SIZE);

//	/* Process the data through the CFFT/CIFFT module */
//	arm_cfft_q15(&arm_cfft_sR_q15_len64, buffPtr, ifftFlag, doBitReverse);
//
//	/* Process the data through the Complex Magnitude Module for
//	  calculating the magnitude at each bin */
//	arm_cmplx_mag_q15(buffPtr, fftOutput, fftSize);

	/* Process the data through the CFFT/CIFFT module */
if(BUFFER_SIZE == 128)
	arm_cfft_q15(&arm_cfft_sR_q15_len64, inputBuffer, ifftFlag, doBitReverse);
else if(BUFFER_SIZE == 64)
	arm_cfft_q15(&arm_cfft_sR_q15_len32, inputBuffer, ifftFlag, doBitReverse);
else if (BUFFER_SIZE == 32)
	arm_cfft_q15(&arm_cfft_sR_q15_len16, inputBuffer, ifftFlag, doBitReverse);
else
	return -1;
	/* Process the data through the Complex Magnitude Module for
	  calculating the magnitude at each bin */
	arm_cmplx_mag_q15(inputBuffer, fftOutput, fftSize);

	/* Calculates maxValue and returns corresponding BIN value */
	arm_max_q15(fftOutput, fftSize, &maxValue, &testIndex);

	return testIndex;
}

data_t ComputeZCR(data_t* buffPtr){
	uint32_t i;
	data_t counter =0;

	for (i =0; i< BUFFER_SIZE -1; i++){
		if ((buffPtr[i]&0x8000) != (buffPtr[i+1]&0x8000))
			counter++;
	}

	return counter;
}
