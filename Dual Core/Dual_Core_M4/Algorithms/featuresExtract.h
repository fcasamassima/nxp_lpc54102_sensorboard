/*
 * featuresExtract.h
 *
 *  Created on: Jul 16, 2015
 *      Author: filippo
 */

#ifndef FEATURESEXTRACT_H_
#define FEATURESEXTRACT_H_

#include "dataBuffer.h"

typedef struct  FeaturesBuffers
{
	data_t Max[3];
	data_t Min[3];
	data_t Avg[3];
	data_t Span[3];
	data_t ZCR[3];
	data_t Std[3];
	data_t SpectrPWR[3];
} features_t;

features_t AccFeat;

features_t GyrFeat;

features_t MagFeat;

features_t PressFeat;

data_t ComputeMean(data_t* buffPtr);
data_t ComputeStdDev(data_t* buffPtr);
data_t ComputeMax(data_t* buffPtr);
data_t ComputeMin(data_t* buffPtr);
data_t ComputeFFT(data_t* buffPtr);

#endif /* FEATURESEXTRACT_H_ */
