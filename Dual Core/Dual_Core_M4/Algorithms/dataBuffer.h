/*
 * dataBuffer.h
 *
 *  Created on: Jul 16, 2015
 *      Author: filippo
 */

#ifndef DATABUFFER_H_
#define DATABUFFER_H_

#include <stdint.h>
#include "global_defines.h"

//#include "sensors.h"

typedef enum _PhysSensorId {
	PHYS_ACCEL_ID = 0,          /**< Accelerometer sensor ID */
	PHYS_GYRO_ID,               /**< Gyro sensor ID */
	PHYS_MAG_ID,                /**< Magnetometer sensor ID */
	PHYS_PRESSURE_ID,           /**< Pressure sensor ID */
	PHYS_PROX_ID,               /**< Proximity sensor ID */
	PHYS_AMBIENT_ID,            /**< Ambient light sensor ID */
    /** @cond INTERNAL */
	PHYS_MAX_ID                 /* internal use to count number of sensors */
	/** @endcond */
} PhysSensorId_t;


#define ACC
#define GYRO
#define MAGNETO
#define PRESS

typedef short int data_t;

typedef enum axes {
	X,
	Y,
	Z
}axesName_t;


typedef struct DataBuffers
{
	data_t X[BUFFER_SIZE];
	data_t Y[BUFFER_SIZE];
	data_t Z[BUFFER_SIZE];
	int currIndex;

}databuff_t;



#ifdef ACC
databuff_t Acc;
int addAccMeasure(data_t NewData[3]);
#endif

#ifdef GYRO
databuff_t Gyr;
int addGyrMeasure(data_t NewData[3]);
#endif

#ifdef MAGNETO
databuff_t Mag;
int addMagMeasure(data_t NewData[3]);
#endif

#ifdef PRESS
//static int PressIndex;
databuff_t Press;
int addPressMeasure(data_t NewData);
#endif

#endif /* DATABUFFER_H_ */
