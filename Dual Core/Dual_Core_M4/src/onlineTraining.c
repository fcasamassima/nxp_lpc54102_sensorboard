/*
 * onlineTraining.c
 *
 *  Created on: Oct 1, 2015
 *      Author: Filippo
 */

#include "board.h"
#include "defns.h"
#include "uart1.h"
#include "onlineTraining.h"


/* Number of bytes to be written to the last sector */
#define IAP_NUM_BYTES_TO_WRITE      NFeat*4*TRAINING_SIZE_PER_CASE // 16 attrib * size of float * number of cases

/* Data array to write to flash */
float src_iap_array_data[TRAINING_SIZE_PER_CASE][NFeat];
static  float** testOut;



uint8_t CheckDataPresence(uint8_t ClassNumber){
	uint32_t* Address;
	int i, DataPresent =0;
	Address = START_ADDR_LAST_SECTOR + IAP_NUM_BYTES_TO_WRITE*ClassNumber;
	for(i=0; i<10; i++){
		if (*(Address+i*4) != 0xFFFFFFFF)
			DataPresent++;
	}
	return DataPresent;
}

uint8_t SaveActivity(uint8_t ClassNo){
	uint8_t ret_code;
	__disable_irq();
	testOut = START_ADDR_LAST_SECTOR;

	/* Prepare to write/erase the last sector */
	ret_code = Chip_IAP_PreSectorForReadWrite(IAP_LAST_SECTOR, IAP_LAST_SECTOR);

	/* Error checking */
	if (ret_code != IAP_CMD_SUCCESS) {
//		DEBUGOUT("Command failed to execute, return code is: %x\r\n", ret_code);
	}

	/* Write to the last sector */
	ret_code = Chip_IAP_CopyRamToFlash(START_ADDR_LAST_SECTOR + IAP_NUM_BYTES_TO_WRITE*ClassNo, src_iap_array_data, IAP_NUM_BYTES_TO_WRITE);

	/* Error checking */
	if (ret_code != IAP_CMD_SUCCESS) {
//		DEBUGOUT("Command failed to execute, return code is: %x\r\n", ret_code);
	}

	/* Re-enable interrupt mode */
	__enable_irq();
	return ret_code;
}
/*
 * Build a new tree using data from FLASH memory (users data acquired with custom training)
 */
void RebuildTreeModel(){
	float * dataArray;
	int * intData;
	dataArray =(float *) START_ADDR_LAST_SECTOR + IAP_NUM_BYTES_TO_WRITE;
	intData = (int * )START_ADDR_LAST_SECTOR + IAP_NUM_BYTES_TO_WRITE;
	if (*intData != -1 ){
		BuildTreePtr(dataArray);
	}
	else{
		//No training Data, send error MSG
		// uart1_snd_str("Using Default Training\r\n");
	}

}

uint8_t EraseModel(void){

	uint8_t ret_code;
	/* Disable interrupt mode so it doesn't fire during FLASH updates */
	__disable_irq();

	/* IAP Flash programming */
	/* Prepare to write/erase the last sector */
	ret_code = Chip_IAP_PreSectorForReadWrite(IAP_LAST_SECTOR, IAP_LAST_SECTOR);

	/* Error checking */
	if (ret_code != IAP_CMD_SUCCESS) {
//		DEBUGOUT("Command failed to execute, return code is: %x\r\n", ret_code);
	}

	/* Erase the last sector */
	ret_code = Chip_IAP_EraseSector(IAP_LAST_SECTOR, IAP_LAST_SECTOR);

	/* Error checking */
	if (ret_code != IAP_CMD_SUCCESS) {
//		DEBUGOUT("Command failed to execute, return code is: %x\r\n", ret_code);
	}

	/* Re-enable interrupt mode */
	__enable_irq();

	return ret_code;
}
