/*
===============================================================================
 Name        : SensorInterface_Multicore_M4.c
 Author      : $(author)
 Version     :
 Copyright   : $(copyright)
 Description : main definition
===============================================================================
*/


#include "board.h"
#include "utilities.h"
#include "interrupts.h"
#include <cr_section_macros.h>
#include "boot_multicore_slave.h"
#include "uart1.h"

#define msg "Tree computation completed \r\n"



/*****************************************************************************
 * Private functions
 ****************************************************************************/

extern bool processingRequest;

int main(void) {
	// Read clock settings and update SystemCoreClock variable
	SystemCoreClockUpdate();

	// Set up and initialize all required blocks and
	// functions related to the board hardware
	Board_Init();

	//Set the clock to IRC @12MHz
	setupClocking();
	SystemCoreClockUpdate();

	// Set the LED to the state of "On"
	Board_LED_Set(1, true);

	//Initialize Mailbox and GPIO interrupts for M4
	// look at interrupts.c for ISRs
	initInterrupts();

	// If classifier is used, at this stage the model is created
	// look at c50.c for classifier
#ifdef USE_CLASSIFIER
	BuildTree(); //This is the default tree that is build first
	RebuildTreeModel(); //If there are data, a custom tree is build
	if ((CoreDebug->DHCSR & CoreDebug_DHCSR_C_DEBUGEN_Msk)==CoreDebug_DHCSR_C_DEBUGEN_Msk) {
		//PrintStats(); //If debugger connected, print stats
	}
	else {	       // Action to take when debug connection inactive
		 }
	FreeData(); // data to build the model were saved in SRAM, after mode creation are no more needed

#endif

//Initialize USART if used
#ifdef USE_USART
	debug_uart_init();
	uart_puts(msg);
#endif

#if defined (__MULTICORE_MASTER_SLAVE_M0SLAVE) || \
	defined (__MULTICORE_MASTER_SLAVE_M4SLAVE)
	//Give M0 control of LP modes
	(*(volatile uint32_t *)CPUCTRL_ADDR) = 0xC0C4000D;
	//Boot the M0
	boot_multicore_slave();
#endif

#ifdef USE_BLE
	int var;
	btle_init(); //Init the BTLE module
	uart1_init();
	uart1_snd_str("gfu 9 conn_gpio\r\n");
	for (var = 0; var < 10000; ++var) {
	}
	uart1_snd_str("save\r\n");
	for (var = 0; var < 10000; ++var) {
	}
	uart1_snd_str("reboot\r\n");
	for (var = 0; var < 1000000; ++var) {
	}

	CleanRXBuff();
#endif



	static bool streamModeInit = false;
	// Enter an infinite loop, just going to sleep
	while(1) {
#ifdef USE_BLE
		//Check if BLE is connected to a device, if so, enable stream mode
		if(Chip_GPIO_GetPinState(LPC_GPIO,0,10) == true && streamModeInit == false){
			uart1_snd_str("str\r\n"); // When in UART mode, this command puts the BTLE module in stream mode
			streamModeInit = true;
			CleanRXBuff();
		}
#endif
		//flag is set in ISR (interrupt.c)
		if (processingRequest == true){
			processData();
			processingRequest = false;
		}
		else{
			__WFI();
		}
    }
    return 0 ;
}
