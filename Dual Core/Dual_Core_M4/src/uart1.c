#include "board.h"
//#include "basic.h"
#define TXBUFSIZE 16
#define RXBUFSIZE 16
static uint8_t txbuf[TXBUFSIZE];
uint8_t rxbuf[RXBUFSIZE];
static uint8_t txwptr=0;
static uint8_t txrptr=0;
static uint8_t rxrptr=0;
uint8_t rxwptr=0;


uint8_t uart1_rcv(){
	while(rxwptr==rxrptr) __WFI(); //empty
	uint8_t r=rxbuf[rxrptr];
	rxrptr=(rxrptr+1)%RXBUFSIZE;
	return r;
}
void uart1_snd(uint8_t c){
	uint8_t nxt_wptr= (txwptr+1)%TXBUFSIZE;
	while(nxt_wptr==txrptr) __WFI(); //full
	txbuf[txwptr]=c;
	txwptr=nxt_wptr;
	LPC_USART1->INTENSET=(1<<2);//TXRDY
}

void uart1_snd_str(char *c){
	while(c[0]!='\0'){
		uart1_snd(c[0]);
		c++;
	}
}
void uart1_rcv_str(char *c,uint32_t size){
	uint32_t cnt=0;
	while(1){
		c[cnt]=uart1_rcv();
		switch(c[cnt]){
		case '\n': c[cnt+1]=0; return;
		case '\r': c[cnt+1]=0; return;
		case '\0': return;
		}
		cnt++;
		if (cnt==size-1) {
			c[cnt]=0;
			return;
		}
	}
}

void UART1_IRQHandler(void) {
	uint32_t stat=LPC_USART1->STAT;
	if (stat&1) {
		uint32_t rxdat=LPC_USART1->RXDAT;
//		printf("uart1 rcv %x\n",rxdat);
		uint8_t nxt_wptr=(rxwptr+1)%RXBUFSIZE;
		if (nxt_wptr!=rxrptr) {
			rxbuf[rxwptr]=rxdat;
			rxwptr=nxt_wptr;
		}
	}
	if (stat&0x4){
		if (txwptr==txrptr) {
//			printf("uart1 snd disable\n");
			LPC_USART1->INTENCLR=(1<<2);//TXRDY
		} else {
//			printf("uart1 snd %x\n",txbuf[txrptr]);
			LPC_USART1->TXDAT=txbuf[txrptr];
			txrptr=(txrptr+1)%TXBUFSIZE;
		}
	}
//	printf("uart1 irq %x\n",stat);
}

#define UART_BAUD_RATE     115200	/* Required UART Baud rate */
#define UART_BUAD_ERR      1/* Percentage of Error allowed in baud */
#define RAMBLOCK_H          60
static uint32_t  drv_mem[RAMBLOCK_H];
static UART_HANDLE_T *hUART;

void uart1_init(){
	int i;
//	UART_CFG_T cfg;
//	UART_BAUD_T baud;
	//setup pins
	LPC_IOCON->PIO[0][5]=IOCON_FUNC1|IOCON_DIGITAL_EN|IOCON_INPFILT_OFF|IOCON_MODE_INACT; //U1_RXD
	LPC_IOCON->PIO[1][10]=IOCON_FUNC2|IOCON_DIGITAL_EN|IOCON_INPFILT_OFF|IOCON_MODE_INACT; //U1_TXD
	//LPC_IOCON->PIO[0][25]=IOCON_FUNC2|IOCON_DIGITAL_EN|IOCON_INPFILT_OFF|IOCON_MODE_INACT; //U1_CTS
	//LPC_IOCON->PIO[1][11]=IOCON_FUNC2|IOCON_DIGITAL_EN|IOCON_INPFILT_OFF|IOCON_MODE_INACT; //U1_RTS

//	Chip_SYSCON_Enable_ASYNC_Syscon(true);	/* Enable Async APB */
//	Chip_Clock_SetAsyncSysconClockDiv(1);	/* Set Async clock divider to 1 */
//
//	hUART = ROM_UART_Init(drv_mem, LPC_USART1_BASE, 0);
//
//	/* Set up baudrate parameters */
//	baud.clk = Chip_Clock_GetAsyncSyscon_ClockRate();	/* Clock frequency */
//	baud.baud = UART_BAUD_RATE;	/* Required baud rate */
//	baud.ovr = 0;	/* Set the oversampling to the recommended rate */
//	baud.mul = baud.div = 0;
//
//	if (ROM_UART_CalBaud(&baud) != LPC_OK) {
//		/* Unable to calculate the baud rate parameters */
//		while (1) {}
//	}
//
//	/* Set fractional control register */
//	Chip_SYSCON_SetUSARTFRGCtrl(baud.mul, 255);
//
//
//	/* Configure the UART */
//	cfg.cfg = UART_CFG_8BIT;
//	cfg.div = baud.div;	/* Use the calculated div value */
//	cfg.ovr = baud.ovr;	/* Use oversampling rate from baud */
//	cfg.res = UART_BIT_DLY(UART_BAUD_RATE);
//
//
//	/* Configure the UART */
//	ROM_UART_Configure(hUART, &cfg);
//
//




//	//clock & do reset
//	LPC_ASYNC_SYSCON->ASYNCAPBCLKSELB=2; //follow pll
//	LPC_ASYNC_SYSCON->ASYNCCLKDIV=1; //100/10 =10MHz
//
//	LPC_ASYNC_SYSCON->FRGCTRL=0xff+(207<<8); //100MHz/(1+207/256) *(1/16 osr)*(1/30 brg)=115191
//
//	LPC_ASYNC_SYSCON->ASYNCPRESETCTRLSET=(1<<2); //reset uart1
//	LPC_ASYNC_SYSCON->ASYNCPRESETCTRLSET=(1<<15); //reset frg
//
//	LPC_ASYNC_SYSCON->ASYNCAPBCLKCTRLSET=(1<<2); //enable uart1 clk
//	LPC_ASYNC_SYSCON->ASYNCAPBCLKCTRLSET=(1<<15); //enable frg clk
//
//	LPC_ASYNC_SYSCON->ASYNCPRESETCTRLCLR=(1<<2); //reset release uart
//	LPC_ASYNC_SYSCON->ASYNCPRESETCTRLCLR=(1<<15); //reset release uart
//
//	LPC_USART1->BRG=29;
//	LPC_USART1->OSR=15;
//	LPC_USART1->CFG=1<<2; //8n1 async


	LPC_ASYNC_SYSCON->ASYNCPRESETCTRLSET=(1<<2); //reset uart1
	LPC_ASYNC_SYSCON->ASYNCPRESETCTRLSET=(1<<15); //reset frg

	LPC_ASYNC_SYSCON->ASYNCAPBCLKCTRLSET=(1<<2); //enable uart1 clk
	LPC_ASYNC_SYSCON->ASYNCAPBCLKCTRLSET=(1<<15); //enable frg clk

	LPC_ASYNC_SYSCON->ASYNCPRESETCTRLCLR=(1<<2); //reset release uart
	LPC_ASYNC_SYSCON->ASYNCPRESETCTRLCLR=(1<<15); //reset release uart


	LPC_USART1->BRG=0x04;
	LPC_USART1->OSR=0x0D;
	LPC_USART1->CFG=1<<2; //8n1 async


	NVIC_EnableIRQ(UART1_IRQn); //priority?
	LPC_USART1->CFG|=1; //enable
	LPC_USART1->INTENSET=1; //RXRDY
	LPC_USART1->CTL=2; //break
	for (i=0; i< 20000; i++);
	LPC_USART1->CTL=0; //break

}
