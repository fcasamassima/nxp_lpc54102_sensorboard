/*
 * utilities.c
 *
 *  Created on: Sep 16, 2015
 *      Author: filippo
 */

#include "utilities.h"

void initInterrupts(){
	/* Get the mailbox identifiers to the core this code is running and the
	   other core */
	myCoreBox = MAILBOX_CM4;
	otherCoreBox = MAILBOX_CM0PLUS;

	/* Initialize mailbox with initial mutex free (master core only) */
	Chip_MBOX_Init(LPC_MBOX);
//	mutexGive();

	/* Enable mailbox interrupt */
	// NVIC_EnableIRQ(MAILBOX_IRQn); // If this IRQ is enabled, M4 will wake every interrupt

	Chip_PININT_Init(LPC_PININT);

	/* Configure GPIO pin as input */
	Chip_GPIO_SetPinDIROutput(LPC_GPIO, GPIO_PININT_PORT, GPIO_PININT_PIN);

	/* Configure pin interrupt selection for the GPIO pin in Input Mux Block */
	Chip_INMUX_PinIntSel(GPIO_PININT_INDEX, GPIO_PININT_PORT, GPIO_PININT_PIN);

	/* Configure channel interrupt as edge sensitive and falling edge interrupt */
	Chip_PININT_ClearIntStatus(LPC_PININT, PININTCH(GPIO_PININT_INDEX));
	Chip_PININT_SetPinModeEdge(LPC_PININT, PININTCH(GPIO_PININT_INDEX));
	Chip_PININT_EnableIntLow(LPC_PININT, PININTCH(GPIO_PININT_INDEX));


	/* Enable interrupt in the NVIC */
	NVIC_EnableIRQ(PININT_NVIC_NAME);

	/* Enable wakeup for PININT0 */
	// Chip_SYSCON_EnableWakeup(SYSCON_STARTER_PINT4);
}


void setupClocking(void)
{
	/* Set main clock source to the IRC clock  This will drive 24MHz
	   for the main clock and 24MHz for the system clock */
	Chip_Clock_SetMainClockSource(SYSCON_MAINCLKSRC_IRC);

	/* Make sure the PLL is off */
	Chip_SYSCON_PowerDown(SYSCON_PDRUNCFG_PD_SYS_PLL);

    /* Wait State setting TBD */
	/* Setup FLASH access to 2 clocks (up to 20MHz) */
    Chip_SYSCON_SetFLASHAccess(FLASHTIM_20MHZ_CPU);

	/* Set system clock divider to 1 */
	Chip_Clock_SetSysClockDiv(1);

    /* ASYSNC SYSCON needs to be on or all serial peripheral won't work.
	   Be careful if PLL is used or not, ASYNC_SYSCON source needs to be
	   selected carefully. */
	Chip_SYSCON_Enable_ASYNC_Syscon(true);
	Chip_Clock_SetAsyncSysconClockDiv(1);
	Chip_Clock_SetAsyncSysconClockSource(SYSCON_ASYNC_IRC);
}



/*
 * Initialization function for the BTLE module on the sensor shield
 *
 * The BTLE module has 2 pins connected to the MCU. One of them for
 * switching between command or stream mode, the other one to factory
 * reset the BTLE module.
 */
void btle_init(void)
{
	//Put P1_9 (BTLE_CMD_DAT used for factory nReset) in GPIO output mode
	LPC_IOCON->PIO[BTLE_CMD_DAT_PORT][BTLE_CMD_DAT_PIN]=IOCON_FUNC0|IOCON_MODE_INACT|IOCON_GPIO_MODE|IOCON_DIGITAL_EN;
	Chip_GPIO_SetPinDIROutput(LPC_GPIO, BTLE_CMD_DAT_PORT, BTLE_CMD_DAT_PIN);
	Chip_GPIO_SetPinState(LPC_GPIO, BTLE_CMD_DAT_PORT, BTLE_CMD_DAT_PIN, false); //1=CMD / 0=STRM

	//Put P0_10 (BTLE_CONN used for switching between command and stream mode) in GPIO output mode
	LPC_IOCON->PIO[BTLE_CONN_PORT][BTLE_CONN_PIN]=IOCON_FUNC0|IOCON_MODE_INACT|IOCON_GPIO_MODE|IOCON_DIGITAL_EN;
	Chip_GPIO_SetPinDIROutput(LPC_GPIO, BTLE_CONN_PORT, BTLE_CONN_PIN);
	Chip_GPIO_SetPinState(LPC_GPIO, BTLE_CONN_PORT, BTLE_CONN_PIN, true); //Set it to true to NOT factory reset

	//BTLE module user uart is connected to UART1 of the board. Rx, Tx, RTS, CTS
}
