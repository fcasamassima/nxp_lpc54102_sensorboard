#ifndef UART1_H
#define UART1_H
void uart1_init();
void uart1_snd(uint8_t c);
uint8_t uart1_rcv();

void uart1_snd_str(char *c);
void uart1_rcv_str(char *c,uint32_t size);
#endif
