/*
 * ineterrupts.h
 *
 *  Created on: Sep 16, 2015
 *      Author: filippo
 */

#ifndef INTERRUPTS_H_
#define INTERRUPTS_H_
#include "board.h"
#include "featuresExtract.h"
#include "utilities.h"
#include "defns.h"


#define Ram0_64
#ifdef Ram0_64
//#define ACCPtrAddr       	0x200FFF0
#define ACCPtrAddr       	0x2017FF0
#endif


#define READ_ACC() 			(*(volatile uint32_t *)ACCPtrAddr)

void processData(void);

#endif /* INTERRUPTS_H_ */
