/*
 * utilities.h
 *
 *  Created on: Sep 16, 2015
 *      Author: filippo
 */

#ifndef UTILITIES_H_
#define UTILITIES_H_
#include "board.h"

#if defined(BOARD_NXP_LPCXPRESSO_54102)
/* GPIO pin for PININT interrupt */
#define GPIO_PININT_PIN     2	/* GPIO pin number mapped to PININT */
#define GPIO_PININT_PORT    0	/* GPIO port number mapped to PININT */
#define GPIO_PININT_INDEX   PININTSELECT4	/* PININT index used for GPIO mapping */
#define PININT_IRQ_HANDLER  PIN_INT4_IRQHandler	/* GPIO interrupt IRQ function name */
#define PININT_NVIC_NAME    PIN_INT4_IRQn	/* GPIO interrupt NVIC interrupt name */
#else
#error "PININT not configured for this example"
#endif /* defined (BOARD_NXP_LPCXPRESSO_54102) */

#define CPUCTRL_ADDR       0x40000300
#define READ_CPUCTRL()     (*(volatile uint32_t *)CPUCTRL_ADDR)
#define WRITE_CPUCTRL(val) ((*(volatile uint32_t *)CPUCTRL_ADDR) = (val))


/* BLUETOOTH */
#define BTLE_CMD_DAT_PIN 9
#define BTLE_CMD_DAT_PORT 1
#define BTLE_CONN_PIN 10
#define BTLE_CONN_PORT 0

#define U1_TXD_PORT 1
#define U1_RXD_PORT 0
#define U1_RTS_PORT 1
#define U1_CTS_PORT 0
#define U1_TXD_PIN 10
#define U1_RXD_PIN 5
#define U1_RTS_PIN 11
#define U1_CTS_PIN 25


static MBOX_IDX_T myCoreBox, otherCoreBox;


void setupClocking(void);
void initInterrupts(void);
void btle_init(void);

#endif /* UTILITIES_H_ */
