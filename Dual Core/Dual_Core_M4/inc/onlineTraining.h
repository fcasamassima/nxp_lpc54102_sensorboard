/*
 * onlineTraining.h
 *
 *  Created on: Oct 2, 2015
 *      Author: filippo
 */

#ifndef ONLINETRAINING_H_
#define ONLINETRAINING_H_

typedef unsigned char 	uint8_t ;
/* Last sector address */
#define START_ADDR_LAST_SECTOR      0x00078000

/* LAST SECTOR */
#define IAP_LAST_SECTOR             15

#define TRAINING_SIZE_PER_CASE		16  //number of cases per activity


/* Size of each sector */
#define SECTOR_SIZE                 32768

/* Number elements in array */
#define ARRAY_ELEMENTS          (IAP_NUM_BYTES_TO_WRITE / sizeof(float))
uint8_t CheckDataPresence(uint8_t ClassNumber);

#endif /* ONLINETRAINING_H_ */
