/*************************************************************************/
/*									 */
/*  Copyright 2010 Rulequest Research Pty Ltd.				 */
/*									 */
/*  This file is part of C5.0 GPL Edition, a single-threaded version	 */
/*  of C5.0 release 2.07.						 */
/*									 */
/*  C5.0 GPL Edition is free software: you can redistribute it and/or	 */
/*  modify it under the terms of the GNU General Public License as	 */
/*  published by the Free Software Foundation, either version 3 of the	 */
/*  License, or (at your option) any later version.			 */
/*									 */
/*  C5.0 GPL Edition is distributed in the hope that it will be useful,	 */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of	 */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU	 */
/*  General Public License for more details.				 */
/*									 */
/*  You should have received a copy of the GNU General Public License	 */
/*  (gpl.txt) along with C5.0 GPL Edition.  If not, see 		 */
/*									 */
/*      <http://www.gnu.org/licenses/>.					 */
/*									 */
/*************************************************************************/



/*************************************************************************/
/*									 */
/*	Main routine, C5.0						 */
/*	------------------						 */
/*									 */
/*************************************************************************/


#include <signal.h>


#include "defns.h"
#include "extern.h"
#include "onlineTraining.h"


int BuildTree()
/*  ----  */
{

    Of = stdout;

    GetNames();

    Progress(-1.0);

    /*  Allocate space for SomeMiss[] and SomeNA[] */

    SomeMiss = AllocZero(MaxAtt+1, Boolean);
    SomeNA   = AllocZero(MaxAtt+1, Boolean);


    GetData();


    /*  Build decision trees  */

    if ( ! BOOST )
    {
	TRIALS = 1;
    }

    InitialiseTreeData();
	ConstructClassifiers();
	Progress(-TRIALS * (MaxCase+1.0));


//	PrintTree(Pruned[0], T_Tree);
//	Evaluate(CMINFO | USAGEINFO);

//	Tree New_Tree;
//	New_Tree = CopyTree(Pruned[0]);
//	fprintf(Of, T_Time, ExecTime() - StartTime);



#ifdef VerbOpt
    Cleanup();
#endif

    return 0;
}


/*
 * Construct decision tree taking data from pointer (can be a pinter in FLASH)
 *
 */
int BuildTreePtr(float *dataArray){
	int i =0, NumClasses=0;;
	Of = stdout;

	GetNames();
	Progress(-1.0);

	for (i=0; i < 4096; i = i+4){
		if (dataArray[i] != dataArray[i]) //detect if it is a NAN
			break;
	}
	NumClasses = i/(NFeat*TRAINING_SIZE_PER_CASE);

	GetDataFromPtr(dataArray,NumClasses);
	if ( ! BOOST )
	{
		TRIALS = 1;
	}

	SomeMiss = AllocZero(MaxAtt+1, Boolean);
	SomeNA   = AllocZero(MaxAtt+1, Boolean);


	InitialiseTreeData();
	ConstructClassifiers();
	Progress(-TRIALS * (MaxCase+1.0));

}

void PrintStats(){
	PrintTree(Pruned[0], T_Tree);
	Evaluate(CMINFO | USAGEINFO);
}

char* ClssifyFeatures(float * feat){


	DataRec	DVec;
	Attribute	Att;
	ClassNo PredClass;
	static ClassNo OldPredClass = 0;
	static int countFilter =0;
	Case = Alloc(1, DataRec);
	MaxCase =0;

	Case[0] = DVec = NewCase();
	ForEach(Att, 1, MaxAtt)
	{
		CVal(DVec, Att) = feat[Att];

	}
	// Class(DVec) = Df[MaxCase][0];


	PredClass = TreeClassify(Case[0], Pruned[0]);

	FreeData();
	if (OldPredClass != PredClass){
		countFilter ++;
		// Low pass filter for activity transitions
		if (countFilter > 4){
			OldPredClass = PredClass;
			countFilter=0;
//			return ClassName[OldPredClass];
		}


	}
//	else{
//		return "+ "; //Return the + sign
//	}
	return ClassName[OldPredClass];
}


