/*************************************************************************/
/*									 */
/*  Copyright 2010 Rulequest Research Pty Ltd.				 */
/*									 */
/*  This file is part of C5.0 GPL Edition, a single-threaded version	 */
/*  of C5.0 release 2.07.						 */
/*									 */
/*  C5.0 GPL Edition is free software: you can redistribute it and/or	 */
/*  modify it under the terms of the GNU General Public License as	 */
/*  published by the Free Software Foundation, either version 3 of the	 */
/*  License, or (at your option) any later version.			 */
/*									 */
/*  C5.0 GPL Edition is distributed in the hope that it will be useful,	 */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of	 */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU	 */
/*  General Public License for more details.				 */
/*									 */
/*  You should have received a copy of the GNU General Public License	 */
/*  (gpl.txt) along with C5.0 GPL Edition.  If not, see 		 */
/*									 */
/*      <http://www.gnu.org/licenses/>.					 */
/*									 */
/*************************************************************************/



/*************************************************************************/
/*									 */
/*	Get cases from data file					 */
/*	------------------------					 */
/*									 */
/*************************************************************************/


#include "defns.h"
#include "extern.h"
#include "training_data.h"






DataRec GetDataRec_Array(float (* Df)[NFeat], int w, int h ,Boolean Train){
    DataRec	DVec;
    Attribute	Att;
    if (MaxCase > w)
    	return NULL;
	Case[MaxCase] = DVec = NewCase();
	ForEach(Att, 1, MaxAtt)
	{
	    CVal(DVec, Att) = Df[MaxCase][Att];
	    if(Df[MaxCase][Att]== 0xFFFFFFFF)
	    	return NULL;

	}
	Class(DVec) = Df[MaxCase][0];
	return DVec;
}


/*************************************************************************/
/*																	*/
/*	Read raw cases from array										*/
/*																	*/
/*	On completion, cases are stored in array Case in the form	 	*/
/*	of vectors of attribute values, and MaxCase is set to the	 	*/
/*	number of data cases.						 					*/
/*									 								*/
/*************************************************************************/

void GetData(){


    DataRec	DVec;
    CaseNo	CaseSpace;
    MaxCase = MaxLabel = CaseSpace = 0;
    Case = Alloc(1, DataRec);	/* for error reporting */

    while ( (DVec = GetDataRec_Array(data, sizeof(data)/4/NFeat-1 ,NFeat,false)) )
    {
    	if ( MaxCase >= CaseSpace )
    	{
    		CaseSpace += 512;
    		Realloc(Case, CaseSpace+1, DataRec);
    	}
    	Case[MaxCase] = DVec;
    	MaxCase++;
    }
    MaxCase--;

}


void GetDataFromPtr(float * dataPtr, int NumClasses){
    DataRec	DVec;
    CaseNo	CaseSpace;
    MaxCase = MaxLabel = CaseSpace = 0;
    Case = Alloc(1, DataRec);	/* for error reporting */

    while ( (DVec = GetDataRec_Array(dataPtr, 16*NumClasses-1 ,NFeat,false)) )
    {
    	if ( MaxCase >= CaseSpace )
    	{
    		CaseSpace += 512;
    		Realloc(Case, CaseSpace+1, DataRec);
    	}
//    	if(DVec[1]._cont_val == 0x7fffffff)
//    		break;
//    	if(DVec[1]._cont_val == 0xffffffff)
//    		break;
//    	if(DVec[1]._cont_val == -nan(0))
//    		break;
    	Case[MaxCase] = DVec;
    	MaxCase++;
    }
    MaxCase--;

}




/*************************************************************************/
/*									 */
/*	Free case space							 */
/*									 */
/*************************************************************************/
void FreeData()
/*   --------  */
{
    FreeCases();

    FreeUnlessNil(IgnoredVals);				IgnoredVals = Nil;
							IValsSize = 0;

    Free(Case);						Case = Nil;

    MaxCase = -1;
}

