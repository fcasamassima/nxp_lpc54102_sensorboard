/*************************************************************************/
/*									 */
/*  Copyright 2010 Rulequest Research Pty Ltd.				 */
/*									 */
/*  This file is part of C5.0 GPL Edition, a single-threaded version	 */
/*  of C5.0 release 2.07.						 */
/*									 */
/*  C5.0 GPL Edition is free software: you can redistribute it and/or	 */
/*  modify it under the terms of the GNU General Public License as	 */
/*  published by the Free Software Foundation, either version 3 of the	 */
/*  License, or (at your option) any later version.			 */
/*									 */
/*  C5.0 GPL Edition is distributed in the hope that it will be useful,	 */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of	 */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU	 */
/*  General Public License for more details.				 */
/*									 */
/*  You should have received a copy of the GNU General Public License	 */
/*  (gpl.txt) along with C5.0 GPL Edition.  If not, see 		 */
/*									 */
/*      <http://www.gnu.org/licenses/>.					 */
/*									 */
/*************************************************************************/



/*************************************************************************/
/*									 */
/*	Get names of classes, attributes and attribute values		 */
/*	-----------------------------------------------------		 */
/*									 */
/*************************************************************************/

//
//#include <sys/types.h>
//#include <sys/stat.h>
#include "defns.h"
#include "extern.h"
#include "training_data.h"

//#define	MAXLINEBUFFER	10000
//int	Delimiter;
//char	LineBuffer[MAXLINEBUFFER], *LBp=LineBuffer;



/*************************************************************************/
/*									 */
/*	Read names of classes, attributes and legal attribute values.	 */
/*	On completion, names are stored in:				 */
/*	  ClassName	-	class names				 */
/*	  AttName	-	attribute names				 */
/*	  AttValName	-	attribute value names			 */
/*	with:								 */
/*	  MaxAttVal	-	number of values for each attribute	 */
/*									 */
/*	Other global variables set are:					 */
/*	  MaxAtt	-	maximum attribute number		 */
/*	  MaxClass	-	maximum class number			 */
/*	  MaxDiscrVal	-	maximum discrete values for an attribute */
/*									 */
/*************************************************************************/


void GetNames()
/*   --------  */
{
#define AttSize 16
AttName	  = AllocZero(AttSize, String);
MaxAttVal	  = AllocZero(AttSize, DiscrValue);
AttValName	  = AllocZero(AttSize, String *);
SpecialStatus = AllocZero(AttSize, char);
AttDef	  = AllocZero(AttSize, Definition);
AttDefUses	  = AllocZero(AttSize, Attribute *);
MaxAtt = 15;

AttName[1] = "Mean X";
AttName[2] = "Mean Y";
AttName[3] = "Mean Z";
AttName[4] = "Std X";
AttName[5] = "Std Y";
AttName[6] = "Std Z";
AttName[7] = "Max X";
AttName[8] = "Max Y";
AttName[9] = "Max Z";
AttName[10] = "Min X";
AttName[11] = "Min Y";
AttName[12] = "Min Z";
AttName[13] = "Range X";
AttName[14] = "Range Y";
AttName[15] = "Range Z";


#define NamesSize 11
MaxClass = NamesSize -1;
ClassName	  = AllocZero(NamesSize, String);
ClassName[0] = 		"N/A      ";
ClassName[SIT] = 	"Seated   ";
ClassName[STAND] = 	"Standing ";
ClassName[WALK] = 	"Walking  ";
ClassName[LIE] = 	"Laying   ";
ClassName[STAIR_UP] = "Stairs Up";
ClassName[STAIR_DWN] = "Stairs Dn";
ClassName[BIKE] = 	"Bicycling";
ClassName[RUN] = 	"Running  ";
ClassName[UNSURE] = "Unknown  ";
ClassName[10] = 	"+        ";

}


