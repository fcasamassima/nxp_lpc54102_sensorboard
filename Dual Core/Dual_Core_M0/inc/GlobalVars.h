/*
 * GlobalVars.h
 *
 *  Created on: Jul 8, 2015
 *      Author: Filippo
 */

#include "global_types.h"

#ifndef GLOBALVARS_H_
#define GLOBALVARS_H_


extern SensorStatus_t PysSensStatus[6];
extern PhysSensorCtrl_1_t PhysSensCtrl[6];
extern KernelTimer_Ctrl_t g_Timer;


#endif /* GLOBALVARS_H_ */
