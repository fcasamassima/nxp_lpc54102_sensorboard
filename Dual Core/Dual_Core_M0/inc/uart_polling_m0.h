/*
 * @brief UART polling example using the ROM API
 *
 * @note
 * Copyright(C) NXP Semiconductors, 2014
 * All rights reserved.
 *
 * @par
 * Software that is described herein is for illustrative purposes only
 * which provides customers with programming information regarding the
 * LPC products.  This software is supplied "AS IS" without any warranties of
 * any kind, and NXP Semiconductors and its licensor disclaim any and
 * all warranties, express or implied, including all implied warranties of
 * merchantability, fitness for a particular purpose and non-infringement of
 * intellectual property rights.  NXP Semiconductors assumes no responsibility
 * or liability for the use of the software, conveys no license or rights under any
 * patent, copyright, mask work right, or any other intellectual property rights in
 * or to any products. NXP Semiconductors reserves the right to make changes
 * in the software without notification. NXP Semiconductors also makes no
 * representation or warranty that such application will be suitable for the
 * specified use without further testing or modification.
 *
 * @par
 * Permission to use, copy, modify, and distribute this software and its
 * documentation is hereby granted, under NXP Semiconductors' and its
 * licensor's relevant copyrights in the software, without fee, provided that it
 * is used in conjunction with NXP Semiconductors microcontrollers.  This
 * copyright, permission, and disclaimer notice must appear in all copies of
 * this code.
 */
#ifdef CORE_M0PLUS


#ifndef USART_POLLING_H
#define USART_POLLING_H

#include "board.h"
#include "romapi_uart.h"

/** @defgroup UART_POLLING_5410X UART polling example using the ROM API
 * @ingroup EXAMPLES_ROM_5410X
 * @include "rom\uart_polling\readme.txt"
 */

/**
 * @}
 */

/*****************************************************************************
 * Private types/enumerations/variables
 ****************************************************************************/
/* UART Driver context memory */
//#define RAMBLOCK_H          60
//static uint32_t  drv_mem[RAMBLOCK_H];
//
///* UART ROM Driver Handle */
//static UART_HANDLE_T *hUART;
//
//#define UART_BAUD_RATE     115200	/* Required UART Baud rate */
//#define UART_BUAD_ERR      1/* Percentage of Error allowed in baud */
//
//#define tmsg1(x) "Type " # x " chars to echo it back\r\n"
//#define tmsg(x) tmsg1(x)
//#define RX_SZ  16
//#define msg   tmsg(RX_SZ)
//#define rmsg  "UART received : "
//
//#define DLY(x) if (1) {volatile uint32_t dl = (x); while (dl--) {}}

/*****************************************************************************
 * Public types/enumerations/variables
 ****************************************************************************/

/* Send data to UART: Blocking */
ErrorCode_t ROM_UART_SendBlock(UART_HANDLE_T hUART, const void *buffer, uint16_t size);

/* Receive data from UART: Blocking */
ErrorCode_t ROM_UART_ReceiveBlock(UART_HANDLE_T hUART, void *buffer, uint16_t size);

char *uart_gets(char *buf, int sz);

const char *uart_puts(const char *buf);

int debug_uart_init( void );

#endif
#endif

