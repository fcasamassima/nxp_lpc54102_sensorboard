/*
 * global_types.h
 *
 *  Created on: Jul 8, 2015
 *      Author: filippo
 */

#ifndef GLOBAL_TYPES_H_
#define GLOBAL_TYPES_H_

typedef struct _Sensor_CTRL {
	int16_t data16[4];			        /**< Sample data */
	uint32_t ts_lastSample;		        /**< Timestamp when last sample is captured */
	uint32_t ts_nextSample;		        /**< Next sample timestamp in Kernel timer ticks */

	uint8_t id;					        /**< PhysSensorId_t type */
	uint8_t enabled;			        /**< flag indicating if the sensor is enabled */
	uint8_t irq_pending;		        /**< irq is pending */
	uint8_t mode;				        /**< Sensor mode, 0 - IRQ, 1 - polling */

	uint32_t period;			        /**< sample period */
	void *libHandle;			        /**< Fusion library's handle for the physical sensor */

} SensorStatus_t;

typedef struct PhysSensorCtrl_1 {
	int32_t(* init) (SensorStatus_t * pSens);                        /**< \see PhysSensorCtrl_init() */
	int32_t(* read) (SensorStatus_t * pSens);                        /**< \see PhysSensorCtrl_read() */
	int32_t(* activate) (SensorStatus_t * pSens, bool enable);       /**< \see PhysSensorCtrl_activate() */
	int32_t(* setDelay) (SensorStatus_t * pSens, uint32_t msec);     /**< \see PhysSensorCtrl_setDelay() */

} PhysSensorCtrl_1_t;

/**
 * @brief Kernel Timer Structure
 */
typedef struct _SH_TIMER_CTRL {
	uint32_t sleep_threshold_us;	/*!< Sleep Threshold in microseconds */
	uint32_t pwrDown_threshold_us;	/*!< Power Down Threshold in microseconds */
	uint32_t bgProc_threshold_us;	/*!< Background Process Threshold in microseconds */

	void(* init) (void);	/*!< Initialization function pointer */
	uint32_t(* GetCurrent) (void);	/*!< Function pointer for reading current time */
	int32_t(* diff) (uint32_t operand1, uint32_t operand2);	/*!< Function pointer for calculating time difference */
	uint32_t(* add) (uint32_t operand1, uint32_t operand2);	/*!< Function pointer for calculating sum of timestamps */
	uint32_t(* GetResolution) (void);	/*!< Function pointer to read timer resolution */
	void(* delayMs) (uint32_t milliseconds);	/*!< Function pointer for delay in milliseconds */
	void(* delayTicks) (uint32_t ticks);	/*!< Function pointer for delay in timer ticks */
	uint32_t(* getTicksFromMs) (uint32_t milliseconds);	/*!< Converts a passed mS count to a kernel tick time */
	uint32_t(* getMsFromTicks) (uint32_t ticks);	/*!< Converts a kernel tick time to a mS count */
	uint32_t(* getUsFromTicks) (uint32_t ticks);	/*!< Converts a kernel tick time to a uS count */
	uint8_t(* GetTimer40HiByte) (void);	/*!< Function pointer  to retrieve higher byte of the 40 bit watchdog count */
	uint32_t(* GetCurrentMsec) (void);	/*!< Function pointer  to retrieve watchdog count in milliseconds */

} KernelTimer_Ctrl_t;

#endif /* GLOBAL_TYPES_H_ */
