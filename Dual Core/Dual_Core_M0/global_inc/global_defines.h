/*
 * global_defines.h
 *
 *  Created on: Sep 16, 2015
 *      Author: filippo
 */

#ifndef GLOBAL_DEFINES_H_
#define GLOBAL_DEFINES_H_

/*
 *   PROCESSING MODE
 */
#define PARALLEL_PROCESSING 1
#define SEQUENTIAL_PROCESSING 0

/**
 *	WORKING MODE
 */
#define FILTER_MODE 2
#define NORMAL_MODE 3

#define BUFFER_SIZE 128		//Choose buffer size, this value can be 64, 128 or 256


#define WORKING_MODE NORMAL_MODE  //Choose processing mode: Normal mode, or Filter mode


#if WORKING_MODE == NORMAL_MODE
	#define PROCESSING_DOWNSAMPLE PROCESSING_6HZ
#else if WORKING_MODE == FILTER_MODE
	#define PROCESSING_DOWNSAMPLE PROCESSING_500HZ
#endif


#define POWER_MODE POWER_SLEEP



/* *******************************
 *
 * For Dual Core Project Only
 *
 * *******************************/
#define PROCESSING_MODE PARALLEL_PROCESSING
//#define USE_USART
//#define USE_BLE
//#define USE_CLASSIFIER
//#define TRAINING

/* *******************************/




#define COMPUTE_FFT


#ifdef USE_CLASSIFIER
#undef TRAINING
#endif

#define PROCESSING_2HZ  30
#define PROCESSING_6HZ  10
#define PROCESSING_33HZ 2

#define PROCESSING_500HZ 1
#define PROCESSING_250HZ 2
#define PROCESSING_125HZ 4



#if WORKING_MODE == FILTER_MODE
#undef BUFFER_SIZE
#define BUFFER_SIZE 105
#endif

#if (WORKING_MODE == FILTER_MODE) && (defined(USE_BLE) || defined(USE_CLASSIFIER))
 #error "Error in Global Defines.h High Data rate cannot be used with Classifier or Bluetooth active"
#endif

#if (WORKING_MODE != FILTER_MODE) && (WORKING_MODE != NORMAL_MODE)
 #error "Error in Global Defines.h Working mode can be only Filter Mode or Normal Mode"
#endif

#if (PROCESSING_MODE != PARALLEL_PROCESSING) && (PROCESSING_MODE != SEQUENTIAL_PROCESSING)
 #error "Error in Global Defines.h Processing mode can only be Parallel or Sequential"
#endif

#if (POWER_MODE == POWER_POWER_DOWN) && defined(USE_BLE)
  #warning "Bluetooth should be used with POWER_SLEEP as POWER_MODE, and not POWER_POWER_DOWN"
#endif

#endif /* GLOBAL_DEFINES_H_ */
