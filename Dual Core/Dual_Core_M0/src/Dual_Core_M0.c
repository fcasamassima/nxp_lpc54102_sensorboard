/*
===============================================================================
 Name        : SensorInterface_Multicore_M0.c
 Author      : $(author)
 Version     :
 Copyright   : $(copyright)
 Description : main definition
===============================================================================
*/

//#define DEBUG_PRINTF
#if defined (__USE_LPCOPEN)
#if defined(NO_BOARD_LIB)
#include "chip.h"
#else
#include "board.h"
#ifdef DEBUG_PRINTF
#include "uart_polling_m0.h"
#endif
#include "sensorhub.h"
#include "sensorhubBoard.h"
#include "sensors.h"
//#include "hostif.h"
#include "sensacq_i2c.h"
#include "stopwatch.h"
#include "global_types.h"
#include "GlobalVars.h"
#include "global_defines.h"
#include "dataBuffer.h"

#endif
#endif

#include <cr_section_macros.h>

#define Ram0_64
#ifdef Ram0_64
//#define ACCPtrAddr       	0x200FFF0
#define ACCPtrAddr       	0x2017FF0
#endif
//#define ACC_ADDR            (*(volatile uint32_t *)ACCPtrAddr)
#define WRITE_ACC(val) 		((*(volatile uint32_t *)ACCPtrAddr) = (val))




#if defined (__MULTICORE_MASTER_SLAVE_M0SLAVE) || \
    defined (__MULTICORE_MASTER_SLAVE_M4SLAVE)
#include "boot_multicore_slave.h"
#endif

int32_t inactivePeriod = 0;
uint8_t dataReady=0;

SensorStatus_t PysSensStatus[6];
PhysSensorCtrl_1_t PhysSensCtrl[6];
KernelTimer_Ctrl_t g_Timer;


void HardFault_Handler(void){
	//Set red LED on
	Board_LED_Set(1, false);
	Board_LED_Set(0, true);
#ifdef DEBUG_PRINTF
	uart_puts("Hard Fault \r\n");
#endif

	while(1);
}

// This flag is used to let know the M0 if the M4 is still running and postpone deep sleep
bool CortexM4Running = false;
void MAILBOX_IRQHandler(void)
{
	CortexM4Running = false;
	Chip_MBOX_ClearValueBits(LPC_MBOX, MAILBOX_CM0PLUS, 0xFFFFFFFF);
}

/* Initialize the interrupts for the physical sensors */
void PhysSensors_IntInit(void)
{
	extern signed char dev_i2c_write(unsigned char dev_addr, unsigned char reg_addr, unsigned char *reg_data, unsigned char cnt);
    uint8_t dat;

	dat = 0x55;
	dev_i2c_write(ADDR_BMI160, 0x53, &dat, 1);
	dat = 0x02;
	dev_i2c_write(ADDR_BMC150A, 0x20, &dat, 1);
	dat = 0x0F;
	dev_i2c_write(ADDR_BMI055G, 0x16, &dat, 1);

    dat = 0x0A; /* Configure IRQ as open drain */
	dev_i2c_write(ADDR_BMI055, 0x20, &dat, 1);

	dat = 0x07;
	dev_i2c_write(ADDR_BMM150, 0x4E, &dat, 1);
	dat = 0x07;
	dev_i2c_write(ADDR_BMC150A, 0x4E, &dat, 1);
}

#define CPUCTRL_ADDR       	0x40000300
#define READ_CPUCTRL()     	(*(volatile uint32_t *)CPUCTRL_ADDR)
#define WRITE_CPUCTRL(val) 	((*(volatile uint32_t *)CPUCTRL_ADDR) = (val))

#define DEV_REG (*(unsigned int *)0x40000300)
int main(void) {
	//At this stage the board has already been initialized by the M4
	uint32_t sleep_ok = 0;
	uint32_t ind = 0;
	WRITE_CPUCTRL(0xC0C4000D);

	// wait a little bit before turning off the Led, j
	for (ind=0; ind< 100000; ind++){
	}

	Board_LED_Set(1, false);

	/* Update core clock variables */
	SystemCoreClockUpdate();

#ifdef DEBUG_PRINTF
	debug_uart_init();
	uart_puts("\r\n USART Initialized, M0 reads sensors\r\n");
#endif


	/* Initialize kernel */
	Kernel_Init();

	/* Init I2C */
	dev_i2c_init();
	/* Configure all IRQ's as tri-state */
	PhysSensors_IntInit();
	/* Initialize sensors structures*/
	PhysSensors_Init();

	SystemCoreClockUpdate();

	//M4 signals M0 when processing is complete
	NVIC_EnableIRQ(MAILBOX_IRQn);

	// Force the counter to be placed into memory
	volatile static int i = 0 ;
	PhysSensors_Enable(PHYS_ACCEL_ID,1);
#if WORKING_MODE == NORMAL_MODE
	PhysSensors_Enable(PHYS_GYRO_ID,1);
	PhysSensors_Enable(PHYS_MAG_ID,1);
#endif
	//	PhysSensors_Enable(PHYS_PRESSURE_ID,1);

	// Write in a memory location the address of the data (can be used to store the address of any structure)
	WRITE_ACC(&Acc);

	while(1) {
		i++;
		//Collect data and start processing if needed
		sleep_ok = Sensor_process();
		if (sleep_ok == 0) {

			//Use sleep time estimator, or force to got to sleep
#ifdef ESTIMETE_SLEEP_TIME
			inactivePeriod = ResMgr_EstimateSleepTime();
			ResMgr_EnterPowerDownMode(inactivePeriod);
#else
			ResMgr_EnterPowerDownMode(5000);
#endif
		}

	}
	return 0 ;
}
